#version 330 core

in vec2 vTexCoord;
out vec4 oFragColour;

uniform sampler2D uTex;
uniform vec2 uPixelScale;
uniform int uFilter;

void main() {
    int kernel_size = 7;
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
    if(uFilter == 1) {
        // if(vTexCoord.x > 0.5) {
        for(int i = -(kernel_size / 2); i <= kernel_size / 2; i++) {
            for(int j = -(kernel_size / 2); j <= kernel_size / 2; j++) {
                if(i == 0 && j == 0) {
                    continue;
                }
                vec2 coord = vec2(vTexCoord.x + i * uPixelScale.x, vTexCoord.y + i * uPixelScale.y);
                color = color + texture(uTex, coord);
            }
        }
        oFragColour = color / (kernel_size * kernel_size - 1);
        // } else {
        // oFragColour = texture(uTex, vTexCoord);
        // }
    }
    else {
        oFragColour = texture(uTex, vTexCoord);
    }
}
