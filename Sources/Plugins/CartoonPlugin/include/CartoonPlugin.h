/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_CartoonPlugin_h
#define packtpub_CartoonPlugin_h


// Local includes
#include "PluginInterface.h"


namespace packtpub {


/**
 * @brief Cartoon image plugin.
 */
class CartoonPlugin : public packtpub::PluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "packtpub.CartoonPlugin")
    Q_INTERFACES(packtpub::PluginInterface)

public:
    CartoonPlugin();
    ~CartoonPlugin() override;
    QString name() override;
    void edit(const cv::Mat& input, cv::Mat& output) override;



};  // class CartoonPlugin


}  // namespace packtpub


#endif  // packtpub_CartoonPlugin_h
