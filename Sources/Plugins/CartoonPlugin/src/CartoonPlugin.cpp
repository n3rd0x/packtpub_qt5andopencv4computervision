/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "CartoonPlugin.h"


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
CartoonPlugin::CartoonPlugin() : PluginInterface() {
}


CartoonPlugin::~CartoonPlugin() {
}


QString CartoonPlugin::name() {
    return "Cartoon";
}


void CartoonPlugin::edit(const cv::Mat& input, cv::Mat& output) {
    qint32 numDown      = 2;
    qint32 numBilateral = 7;

    cv::Mat cpyA, cpyB;
    cv::Mat imgGray, imgEdge;

    cpyA = input.clone();
    for(auto i = 0; i < numDown; i++) {
        cv::pyrDown(cpyA, cpyB);
        cpyA = cpyB.clone();
    }

    for(auto i = 0; i < numBilateral; i++) {
        cv::bilateralFilter(cpyA, cpyB, 9, 9, 7);
        cpyA = cpyB.clone();
    }

    for(auto i = 0; i < numDown; i++) {
        cv::pyrUp(cpyA, cpyB);
        cpyA = cpyB.clone();
    }

    if(input.cols != cpyA.cols || input.rows != cpyA.rows) {
        cv::Rect rect(0, 0, input.cols, input.rows);
        cpyA(rect).copyTo(cpyB);
        cpyA = cpyB;
    }

    cv::cvtColor(input, imgGray, cv::COLOR_RGB2GRAY);
    cv::medianBlur(imgGray, imgGray, 5);

    cv::adaptiveThreshold(imgGray, imgGray, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 9, 2);

    cv::cvtColor(imgGray, imgEdge, cv::COLOR_GRAY2RGB);

    output = cpyA & imgEdge;


    //    cv::GaussianBlur(imgEdge, imgEdge, cv::Size(5, 5), 0);
    //    cv::Mat mask(input.rows, input.cols, CV_8UC3, cv::Scalar(90, 90, 90));
    //    mask   = mask & (~imgEdge);
    //    output = (cpyA & imgEdge) | mask;
}


}  // namespace packtpub
