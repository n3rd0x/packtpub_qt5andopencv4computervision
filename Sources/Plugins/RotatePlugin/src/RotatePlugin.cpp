/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "RotatePlugin.h"


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
RotatePlugin::RotatePlugin() : PluginInterface() {
}


RotatePlugin::~RotatePlugin() {
}


QString RotatePlugin::name() {
    return "Rotate";
}


void RotatePlugin::edit(const cv::Mat& input, cv::Mat& output) {
    qreal angle        = 45.0;
    qreal scale        = 1.0;
    cv::Point2f center = cv::Point(input.cols / 2, input.rows / 2);
    cv::Mat rotMat     = cv::getRotationMatrix2D(center, angle, scale);

    cv::Mat result;
    cv::warpAffine(input, result, rotMat, input.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);

    output = result;
}


}  // namespace packtpub
