/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_RotatePlugin_h
#define packtpub_RotatePlugin_h


// Local includes
#include "PluginInterface.h"


namespace packtpub {


/**
 * @brief Rotate image plugin.
 */
class RotatePlugin : public packtpub::PluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "packtpub.RotatePlugin")
    Q_INTERFACES(packtpub::PluginInterface)

public:
    RotatePlugin();
    ~RotatePlugin() override;
    QString name() override;
    void edit(const cv::Mat& input, cv::Mat& output) override;



};  // class RotatePlugin


}  // namespace packtpub


#endif  // packtpub_RotatePlugin_h
