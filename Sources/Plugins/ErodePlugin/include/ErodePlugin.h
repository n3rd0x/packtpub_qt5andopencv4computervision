/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_ErodePlugin_h
#define packtpub_ErodePlugin_h


// Local includes
#include "PluginInterface.h"


namespace packtpub {


/**
 * @brief Erode image plugin.
 */
class ErodePlugin : public packtpub::PluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "packtpub.ErodePlugin")
    Q_INTERFACES(packtpub::PluginInterface)

public:
    ErodePlugin();
    ~ErodePlugin() override;
    QString name() override;
    void edit(const cv::Mat& input, cv::Mat& output) override;



};  // class ErodePlugin


}  // namespace packtpub


#endif  // packtpub_ErodePlugin_h
