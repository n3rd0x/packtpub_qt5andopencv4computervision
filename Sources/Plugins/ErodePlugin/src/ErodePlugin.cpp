/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "ErodePlugin.h"


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
ErodePlugin::ErodePlugin() : PluginInterface() {
}


ErodePlugin::~ErodePlugin() {
}


QString ErodePlugin::name() {
    return "Erode";
}


void ErodePlugin::edit(const cv::Mat& input, cv::Mat& output) {
    cv::erode(input, output, cv::Mat());
}


}  // namespace packtpub
