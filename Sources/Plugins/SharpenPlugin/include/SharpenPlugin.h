/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_SharpenPlugin_h
#define packtpub_SharpenPlugin_h


// Local includes
#include "PluginInterface.h"


namespace packtpub {


/**
 * @brief Sharpen image plugin.
 */
class SharpenPlugin : public packtpub::PluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "packtpub.SharpenPlugin")
    Q_INTERFACES(packtpub::PluginInterface)

public:
    SharpenPlugin();
    ~SharpenPlugin() override;
    QString name() override;
    void edit(const cv::Mat& input, cv::Mat& output) override;



};  // class SharpenPlugin


}  // namespace packtpub


#endif  // packtpub_SharpenPlugin_h
