# ************************************************************
# Project:  Qt5 and OpenCV 4 Computer Vision
# Ref:      https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
# ************************************************************




# ************************************************************
# Project Details
# ************************************************************
project("SharpenPlugin")




# ************************************************************
# Configurations
# ************************************************************
initialise_local_variable()
set(LOCAL_PATH_RESOURCE "${CMAKE_CURRENT_SOURCE_DIR}/res")




# ************************************************************
# Header Files
# ************************************************************
set(LOCAL_HEADER_FILES
    "${PROJECT_PATH_COMMON_HEADER}/Prerequisites.h"
)
file(GLOB LOCAL_QT_HEADER_FILES ${LOCAL_PATH_HEADER}/*.h)
list(APPEND LOCAL_QT_HEADER_FILES "${PROJECT_PATH_COMMON_HEADER}/PluginInterface.h")




# ************************************************************
# Source Files
# ************************************************************
set(LOCAL_SOURCE_FILES
)
file(GLOB LOCAL_QT_SOURCE_FILES ${LOCAL_PATH_SOURCE}/*.cpp)
list(APPEND LOCAL_QT_SOURCE_FILES "${PROJECT_PATH_COMMON_SOURCE}/PluginInterface.cpp")




# ************************************************************
# Qt Configurations
# ************************************************************
file(GLOB LOCAL_QT_SOURCE_UI_FILES ${LOCAL_PATH_RESOURCE}/ui/*.ui)
file(GLOB LOCAL_QT_SOURCE_QRC_FILES ${LOCAL_PATH_RESOURCE}/qrc/*.qrc)

# Group Qt headers and sources.
set(LOCAL_QT_HEADERS
    ${LOCAL_QT_HEADER_FILES}
)

set(LOCAL_QT_SOURCES
    ${LOCAL_QT_SOURCE_FILES}
)

# Qt generated files.
qt5_wrap_cpp(LOCAL_QT_GENERATED_MOC_FILES ${LOCAL_QT_HEADERS})
qt5_add_resources(LOCAL_QT_GENERATED_QRC_FILES ${LOCAL_QT_SOURCE_QRC_FILES})
qt5_wrap_ui(LOCAL_QT_GENERATED_UI_FILES ${LOCAL_QT_SOURCE_UI_FILES})

# Group files.
source_group("Generated Files\\Moc Files" FILES ${LOCAL_QT_GENERATED_MOC_FILES})
source_group("Generated Files\\Qrc Files" FILES ${LOCAL_QT_GENERATED_QRC_FILES})
source_group("Generated Files\\UI Files" FILES ${LOCAL_QT_GENERATED_UI_FILES})
source_group("Resource Files" FILES ${LOCAL_QT_SOURCE_QRC_FILES})
source_group("UI Files" FILES ${LOCAL_QT_SOURCE_UI_FILES})




# ************************************************************
# Configure Project
# ************************************************************
# Project header files.
set(LOCAL_HEADERS
    ${LOCAL_HEADER_FILES}
    ${LOCAL_GROUP_HEADER_FILES}
    ${LOGURU_HEADER_FILES}
)

# Project source files.
set(LOCAL_SOURCES
    ${LOCAL_SOURCE_FILES}
    ${LOCAL_GROUP_SOURCE_FILES}
    ${LOGURU_SOURCE_FILES}
)

# Project Qt files.
set(LOCAL_QT_FILES
    ${LOCAL_QT_HEADER_FILES}
    ${LOCAL_QT_SOURCE_FILES}
    ${LOCAL_QT_GENERATED_MOC_FILES}
    ${LOCAL_QT_GENERATED_QRC_FILES}
    ${LOCAL_QT_GENERATED_UI_FILES}
)

# Project include directories.
set(LOCAL_INCLUDES
    ${LOCAL_PATH_HEADER}
    ${PROJECT_INCLUDE_DIR}
    ${OPENCV_INCLUDE_DIR}
)

# Project libraries.
set(LOCAL_LIBRARIES
    ${PROJECT_LIBRARIES}
    ${OPENCV_LIBRARIES}
)

# Project includes directories.
include_directories(${LOCAL_INCLUDES})




# ************************************************************
# Binary Output
# ************************************************************
# Generate binary.
add_library(${PROJECT_NAME} SHARED ${LOCAL_HEADERS} ${LOCAL_SOURCES} ${LOCAL_QT_FILES})

# Linking libraries.
target_link_libraries(${PROJECT_NAME} ${LOCAL_LIBRARIES})

# Target options.
set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    DEBUG_POSTFIX ${CMAKE_DEBUG_POSTFIX}
)

# Copy project templates.
copy_project_template()

# Copy the generated plug-in to the output directory.
copy_build_file_to_output_directory(SubPath "/plugins")



# ************************************************************
# Installation
# ************************************************************
install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION "${PROJECT_PATH_INSTALL}"
    LIBRARY DESTINATION "${PROJECT_PATH_INSTALL}"
    ARCHIVE DESTINATION "${PROJECT_PATH_INSTALL}"
)
