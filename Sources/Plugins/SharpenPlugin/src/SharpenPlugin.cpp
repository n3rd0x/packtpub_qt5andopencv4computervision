/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "SharpenPlugin.h"


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
SharpenPlugin::SharpenPlugin() : PluginInterface() {
}


SharpenPlugin::~SharpenPlugin() {
}


QString SharpenPlugin::name() {
    return "Sharpen";
}


void SharpenPlugin::edit(const cv::Mat& input, cv::Mat& output) {
    qint32 intensity = 2;
    cv::Mat smoothed;
    cv::GaussianBlur(input, smoothed, cv::Size(9, 9), 0);
    output = input + (input - smoothed) * intensity;
}


}  // namespace packtpub
