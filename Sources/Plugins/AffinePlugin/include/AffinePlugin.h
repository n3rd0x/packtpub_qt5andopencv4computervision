/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_AffinePlugin_h
#define packtpub_AffinePlugin_h


// Local includes
#include "PluginInterface.h"


namespace packtpub {


/**
 * @brief AffinePlugin image plugin.
 */
class AffinePlugin : public packtpub::PluginInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "packtpub.AffinePluginPlugin")
    Q_INTERFACES(packtpub::PluginInterface)

public:
    AffinePlugin();
    ~AffinePlugin() override;
    QString name() override;
    void edit(const cv::Mat& input, cv::Mat& output) override;



};  // class AffinePlugin


}  // namespace packtpub


#endif  // packtpub_AffinePlugin_h
