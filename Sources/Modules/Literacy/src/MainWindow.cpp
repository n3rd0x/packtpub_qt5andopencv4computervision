/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "ScreenCapture.h"

// Qt includes
#include <QFileDialog>
#include <QMessageBox>
#include <QKeyEvent>
#include <QSplitter>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
    if(mTesseract) {
        mTesseract->End();
        delete mTesseract;
    }
}


void MainWindow::decode(const cv::Mat& scores,
                        const cv::Mat& geometry,
                        qreal scoreThresh,
                        std::vector<cv::RotatedRect>& detections,
                        std::vector<float>& confidences) {
    CV_Assert(scores.dims == 4);
    CV_Assert(geometry.dims == 4);
    CV_Assert(scores.size[0] == 1);
    CV_Assert(scores.size[1] == 1);
    CV_Assert(geometry.size[0] == 1);
    CV_Assert(geometry.size[1] == 5);
    CV_Assert(scores.size[2] == geometry.size[2]);
    CV_Assert(scores.size[3] == geometry.size[3]);

    detections.clear();
    auto height = scores.size[2];
    auto width  = scores.size[3];
    for(auto y = 0; y < height; y++) {
        auto scoresData = scores.ptr<float>(0, 0, y);
        auto x0_data    = geometry.ptr<float>(0, 0, y);
        auto x1_data    = geometry.ptr<float>(0, 1, y);
        auto x2_data    = geometry.ptr<float>(0, 2, y);
        auto x3_data    = geometry.ptr<float>(0, 3, y);
        auto anglesData = geometry.ptr<float>(0, 4, y);
        for(auto x = 0; x < width; ++x) {
            float score = scoresData[x];
            if(score < scoreThresh) {
                continue;
            }

            // Decode a prediction.
            // Multiple by 4 because feature maps are 4 time less than input image.
            auto offsetX = x * 4.0f, offsetY = y * 4.0f;
            auto angle = anglesData[x];
            auto cosA  = std::cos(angle);
            auto sinA  = std::sin(angle);
            auto h     = x0_data[x] + x2_data[x];
            auto w     = x1_data[x] + x3_data[x];

            cv::Point2f offset(offsetX + cosA * x1_data[x] + sinA * x2_data[x],
                               offsetY - sinA * x1_data[x] + cosA * x2_data[x]);
            cv::Point2f p1 = cv::Point2f(-sinA * h, -cosA * h) + offset;
            cv::Point2f p3 = cv::Point2f(-cosA * w, sinA * w) + offset;
            cv::RotatedRect r(0.5f * (p1 + p3), cv::Size2f(w, h), -angle * 180.0f / static_cast<float>(CV_PI));
            detections.push_back(r);
            confidences.push_back(score);
        }
    }
}


cv::Mat MainWindow::detectTextAreas(QImage& image, std::vector<cv::Rect>& areas) {
    // NB! The input image must the multiple with 32.
    // In this case the image must be 320x320 resolution.
    qreal confidence = 0.5;
    qreal nms        = 0.4;
    qint32 inWidth   = 320;
    qint32 inHeight  = 320;
    auto model       = "data/frozen_east_text_detection.pb";

    // Load DNN network.
    if(mNet.empty()) {
        mNet = cv::dnn::readNet(model);
    }

    std::vector<cv::Mat> outs;
    std::vector<std::string> layerNames(2);
    layerNames[0] = "feature_fusion/Conv_7/Sigmoid";
    layerNames[1] = "feature_fusion/concat_3";

    cv::Mat frame = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), image.bytesPerLine()).clone();

    cv::Mat blob;
    cv::dnn::blobFromImage(
        frame, blob, 1.0, cv::Size(inWidth, inHeight), cv::Scalar(123.68, 116.78, 103.94), true, false);
    mNet.setInput(blob);
    mNet.forward(outs, layerNames);

    cv::Mat scores   = outs[0];
    cv::Mat geometry = outs[1];

    std::vector<cv::RotatedRect> boxes;
    std::vector<float> confidences;
    decode(scores, geometry, confidence, boxes, confidences);

    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confidences, confidence, nms, indices);

    // Render detections.
    cv::Point2f ratio(static_cast<float>(frame.cols / inWidth), static_cast<float>(frame.rows / inHeight));
    cv::Scalar green = cv::Scalar(0, 255, 0);

    for(size_t i = 0; i < indices.size(); i++) {
        auto box  = boxes[indices[i]];
        auto area = box.boundingRect();

        area.x *= ratio.x;
        area.width *= ratio.x;
        area.y *= ratio.y;
        area.height *= ratio.y;
        areas.push_back(area);

        cv::rectangle(frame, area, green, 1);

        QString index = QString("%1").arg(i);
        cv::putText(
            frame, index.toStdString(), cv::Point2f(area.x, area.y - 2), cv::FONT_HERSHEY_SIMPLEX, 0.5, green, 1);
    }

    return frame;
}


void MainWindow::initPointers() {
    mImageScene   = nullptr;
    mImageView    = nullptr;
    mMainStatus   = nullptr;
    mCurrentImage = nullptr;
    mTesseract    = nullptr;
}


void MainWindow::initValues() {
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupShortcuts()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mActionExit, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mActionOpen, &QAction::triggered, this, &MainWindow::cbOpenImage);
    connect(mActionSaveImageAs, &QAction::triggered, this, &MainWindow::cbSaveImageAs);
    connect(mActionSaveTextAs, &QAction::triggered, this, &MainWindow::cbSaveTextAs);
    connect(mActionOCR, &QAction::triggered, this, &MainWindow::cbExtractText);
    connect(mActionCapture, &QAction::triggered, this, &MainWindow::cbCaptureScreen);
    return true;
}


bool MainWindow::setupDefaultSettings() {
    return true;
}


bool MainWindow::setupFinalState() {
    resize(800, 600);
    setWindowTitle(qApp->applicationName());
    return true;
}


bool MainWindow::setupShortcuts() {
    QList<QKeySequence> shortcuts;
    shortcuts << (Qt::CTRL + Qt::Key_0);
    mActionOpen->setShortcuts(shortcuts);

    shortcuts.clear();
    shortcuts << (Qt::CTRL + Qt::Key_Q);
    mActionExit->setShortcuts(shortcuts);
    return true;
}


bool MainWindow::setupUis() {
    // Setup UI defined in the designer.
    setupUi(this);

    auto splitter = new QSplitter(Qt::Horizontal, this);
    mImageScene   = new QGraphicsScene(this);
    mImageView    = new QGraphicsView(mImageScene);
    splitter->addWidget(mImageView);

    mEditor = new QTextEdit(this);
    splitter->addWidget(mEditor);

    setCentralWidget(splitter);

    // Setup status bar.
    mMainStatus = new QLabel(mStatusBar);
    mStatusBar->addPermanentWidget(mMainStatus);
    mMainStatus->setText(tr("Literacy is Ready!"));
    return true;
}


void MainWindow::showImage(const QPixmap& pixmap) {
    mImageScene->clear();
    mImageView->resetMatrix();
    mCurrentImage = mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());
}


void MainWindow::showImage(const QString& file) {
    QPixmap image(file);
    showImage(image);
    mCurrentImageFile = file;
    QString status
        = QString("%1, %2x%3, %4 Bytes").arg(file).arg(image.width()).arg(image.height()).arg(QFile(file).size());
    mMainStatus->setText(status);
}


void MainWindow::showImage(const cv::Mat& mat) {
    QImage image(mat.data, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
    QPixmap pixmap = QPixmap::fromImage(image);
    showImage(pixmap);
}


}  // namespace packtpub
