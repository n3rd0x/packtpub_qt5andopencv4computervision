/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "ScreenCapture.h"

// Qt includes
#include <QApplication>
#include <QGuiApplication>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QRect>
#include <QScreen>
#include <QPainter>
#include <QColor>
#include <QRegion>
#include <QShortcut>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
ScreenCapture::ScreenCapture(MainWindow* window) : QWidget(nullptr), mWindow(window) {
    setWindowFlags(Qt::BypassWindowManagerHint | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Tool);

    setAttribute(Qt::WA_DeleteOnClose);
    // setMouseTracking(true);

    mScreen = captureDesktop();
    resize(mScreen.size());
    initShortcuts();
}


ScreenCapture::~ScreenCapture() {
}


QPixmap ScreenCapture::captureDesktop() {
    QRect geometry;
    for(auto itr : QGuiApplication::screens()) {
        geometry = geometry.united(itr->geometry());
    }

    QPixmap pixmap(QApplication::primaryScreen()->grabWindow(
        QApplication::desktop()->winId(), geometry.x(), geometry.y(), geometry.width(), geometry.height()));
    pixmap.setDevicePixelRatio(QApplication::desktop()->devicePixelRatio());
    return pixmap;
}


void ScreenCapture::paintEvent(QPaintEvent* evt) {
    Q_UNUSED(evt)

    QPainter painter(this);
    painter.drawPixmap(0, 0, mScreen);

    QRegion grey(rect());
    if(mP1.x() != mP2.x() && mP1.y() != mP2.y()) {
        painter.setPen(QColor(200, 100, 50, 255));
        painter.drawRect(QRect(mP1, mP2));
        grey = grey.subtracted(QRect(mP1, mP2));
    }

    painter.setClipRegion(grey);
    QColor overlayColour(20, 20, 20, 50);
    painter.fillRect(rect(), overlayColour);
    painter.setClipRect(rect());
}


void ScreenCapture::mousePressEvent(QMouseEvent* evt) {
    mMouseDown = true;
    mP1        = evt->pos();
    mP2        = evt->pos();
    update();
}


void ScreenCapture::mouseMoveEvent(QMouseEvent* evt) {
    if(!mMouseDown) {
        return;
    }
    mP2 = evt->pos();
    update();

    DLOG_S(INFO) << "Mouse (" << mP1.x() << ", " << mP1.y() << ") (" << mP2.x() << ", " << mP2.y() << ")";
}


void ScreenCapture::mouseReleaseEvent(QMouseEvent* evt) {
    mMouseDown = false;
    mP2        = evt->pos();
    update();
}


void ScreenCapture::closeMe() {
    this->close();
    mWindow->showNormal();
    mWindow->activateWindow();
}


void ScreenCapture::confirmCapture() {
    QRect rect(mP1, mP2);
    QPixmap image = mScreen.copy(rect);
    mWindow->showImage(image);
    closeMe();
}


void ScreenCapture::initShortcuts() {
    new QShortcut(Qt::Key_Escape, this, SLOT(closeMe()));
    new QShortcut(Qt::Key_Return, this, SLOT(confirmCapture()));
}


}  // namespace packtpub
