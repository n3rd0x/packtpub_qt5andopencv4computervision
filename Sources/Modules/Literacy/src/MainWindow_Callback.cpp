/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "Prerequisites.h"
#include "MainWindow.h"
#include "ScreenCapture.h"

// Qt includes
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbCaptureScreen() {
    this->setWindowState(this->windowState() | Qt::WindowMinimized);
    QTimer::singleShot(500, this, SLOT(cbStartCapture()));
}


void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbExtractText() {
    if(!mCurrentImage) {
        QMessageBox::information(this, "Information", "No open image.");
        return;
    }

    char* old_ctype = strdup(setlocale(LC_ALL, nullptr));
    setlocale(LC_ALL, "C");
    if(!mTesseract) {
        mTesseract = new tesseract::TessBaseAPI();

        // Initialize tesseract-ocr with English, with specifying tessdata path.
        auto datapath = "data";
        if(mTesseract->Init(datapath, "eng")) {
            QMessageBox::information(this, "Error", "Could not initialize tesseract.");
            return;
        }
    }

    auto pixmap = mCurrentImage->pixmap();
    auto image  = pixmap.toImage();
    image       = image.convertToFormat(QImage::Format_RGB888);

    mTesseract->SetImage(image.bits(), image.width(), image.height(), 3, image.bytesPerLine());
    if(mActionDetectTextAreas->isChecked()) {
        std::vector<cv::Rect> areas;
        cv::Mat newImage = detectTextAreas(image, areas);
        showImage(newImage);

        mEditor->setPlainText("");
        for(auto itr : areas) {
            if(itr.area() > 0) {
                mTesseract->SetRectangle(itr.x, itr.y, itr.width, itr.height);
                auto outText = std::unique_ptr<char>(mTesseract->GetUTF8Text());
                mEditor->setPlainText(mEditor->toPlainText() + outText.get());
            }
            else {
                DLOG_S(INFO) << "The area has some zero values. Skip processing.";
            }
        }
    }
    else {
        auto outText = std::unique_ptr<char>(mTesseract->GetUTF8Text());
        mEditor->setPlainText(outText.get());
    }

    setlocale(LC_ALL, old_ctype);
    free(old_ctype);
}


void MainWindow::cbOpenImage() {
    QFileDialog dialog(this);
    dialog.setWindowTitle("Open Image");
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));
    QStringList filePaths;
    if(dialog.exec()) {
        filePaths = dialog.selectedFiles();
        showImage(filePaths.at(0));
    }
}


void MainWindow::cbSaveImageAs() {
    if(!mCurrentImage) {
        QMessageBox::information(this, "Information", "Noting to save.");
        return;
    }
    QFileDialog dialog(this);
    dialog.setWindowTitle("Save Image As ...");
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));
    QStringList fileNames;
    if(dialog.exec()) {
        fileNames = dialog.selectedFiles();
        if(QRegExp(".+\\.(png|bmp|jpg)").exactMatch(fileNames.at(0))) {
            mCurrentImage->pixmap().save(fileNames.at(0));
        }
        else {
            QMessageBox::information(this, "Error", "Save error: bad format or filename.");
        }
    }
}


void MainWindow::cbSaveTextAs() {
    QFileDialog dialog(this);
    dialog.setWindowTitle("Save Text As ...");
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Text files (*.txt)"));
    QStringList fileNames;
    if(dialog.exec()) {
        fileNames = dialog.selectedFiles();
        if(QRegExp(".+\\.(txt)").exactMatch(fileNames.at(0))) {
            QFile file(fileNames.at(0));
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                QMessageBox::information(this, "Error", "Can't save text.");
                return;
            }
            QTextStream out(&file);
            out << mEditor->toPlainText() << "\n";
        }
        else {
            QMessageBox::information(this, "Error", "Save error: bad format or filename.");
        }
    }
}


void MainWindow::cbStartCapture() {
    auto cap = new ScreenCapture(this);
    cap->show();
    cap->activateWindow();
}


}  // namespace packtpub
