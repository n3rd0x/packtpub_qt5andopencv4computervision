/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



#ifndef packtpub_MainWindow_h
#define packtpub_MainWindow_h


// Qt includes
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QMainWindow>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QTextEdit>

#include <ui_MainWindow.h>

// OpenCV includes
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>

// Tesseract includes
#include <tesseract/baseapi.h>


namespace packtpub {


/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow, protected Ui::MainWindow {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Default constructor.
     * @param parent Parent of this window.
     */
    explicit MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Default destructor.
     */
    virtual ~MainWindow();


    /**
     * @brief Setup the window.
     * @return True on success.
     */
    bool setup();


    /**
     * @brief Show image.
     * @param pixmap Pixmap to process.
     */
    void showImage(const QPixmap& pixmap);




protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Decode.
     * @param scores
     * @param geometry
     * @param scoreThresh
     * @param detections
     * @param confidences
     */
    void decode(const cv::Mat& scores,
                const cv::Mat& geometry,
                qreal scoreThresh,
                std::vector<cv::RotatedRect>& detections,
                std::vector<float>& confidences);


    /**
     * @brief Detect text area.
     * @param image Image to process.
     * @param areas List of areas.
     * @return Matrix containing the result.
     */
    cv::Mat detectTextAreas(QImage& image, std::vector<cv::Rect>& areas);


    /**
     * @brief Initialise pointers.
     */
    void initPointers();


    /**
     * @brief Initialise values.
     */
    void initValues();


    /**
     * @brief Setup connections.
     * @return True on success.
     */
    bool setupConnections();


    /**
     * @brief Set default settings.
     * @return True on success.
     */
    bool setupDefaultSettings();


    /**
     * @brief Finalise the setup.
     * @return True on success.
     */
    bool setupFinalState();


    /**
     * @brief Setup shortcuts.
     * @return True on success.
     */
    bool setupShortcuts();


    /**
     * @brief Setup UIs.
     * @return True on success.
     */
    bool setupUis();


    /**
     * @brief Show image.
     */
    void showImage(const QString& file);
    void showImage(const cv::Mat& mat);


    /**
     * @brief References.
     */
    QGraphicsScene* mImageScene;
    QGraphicsView* mImageView;
    QTextEdit* mEditor;
    QLabel* mMainStatus;

    QString mCurrentImageFile;
    QGraphicsPixmapItem* mCurrentImage;

    tesseract::TessBaseAPI* mTesseract;
    cv::dnn::Net mNet;


protected slots:
    void cbCaptureScreen();
    void cbExit();
    void cbExtractText();
    void cbOpenImage();
    void cbSaveImageAs();
    void cbSaveTextAs();
    void cbStartCapture();


};  // class MainWindow


}  // namespace packtpub


#endif  // packtpub_MainWindow_h
