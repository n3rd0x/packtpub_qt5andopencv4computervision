/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_ScreenCapture_h
#define packtpub_ScreenCapture_h


// Local includes
#include "MainWindow.h"

// Qt includes
#include <QWidget>
#include <QPixmap>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QPoint>


namespace packtpub {


/**
 * @brief Screen capture.
 */
class ScreenCapture : public QWidget {
    Q_OBJECT
public:
    explicit ScreenCapture(MainWindow* window);
    ~ScreenCapture() override;


protected:
    void paintEvent(QPaintEvent* evt) override;
    void mouseMoveEvent(QMouseEvent* evt) override;
    void mousePressEvent(QMouseEvent* evt) override;
    void mouseReleaseEvent(QMouseEvent* evt) override;

    void initShortcuts();
    QPixmap captureDesktop();

    MainWindow* mWindow;
    QPixmap mScreen;
    QPoint mP1;
    QPoint mP2;
    bool mMouseDown;


protected slots:
    void closeMe();
    void confirmCapture();


};  // class ScreenCapture


}  // namespace packtpub


#endif  // packtpub_ScreenCapture_h
