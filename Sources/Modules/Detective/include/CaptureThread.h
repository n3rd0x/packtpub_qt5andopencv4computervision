/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_CaptureThread_h
#define packtpub_CaptureThread_h


// Qt includes
#include <QMutex>
#include <QString>
#include <QThread>

// OpenCV includes
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/dnn.hpp>

// STL includes
#include <vector>


namespace packtpub {


/**
 * @brief Capture threading.
 */
class CaptureThread : public QThread {
    Q_OBJECT
public:
    enum class Classifier : qint32 { FRONTAL_FACE, FRONTAL_CAT, BOSTON_BULL };
    CaptureThread(const QString& id, QMutex* lock);
    ~CaptureThread() override;
    void setRunning(const bool state);
    void takePhoto();

    static void decodeOutLayers(cv::Mat& frame,
                                const std::vector<cv::Mat>& outs,
                                std::vector<int>& outClassIds,
                                std::vector<float>& outConfidences,
                                std::vector<cv::Rect>& outBoxes);
    static std::vector<std::string> getOutputsName(const cv::dnn::Net& net);

    static std::string backendName(cv::dnn::Backend value);
    static std::string targetName(cv::dnn::Target value);


public slots:
    void setBackend(std::pair<int32_t, int32_t> value);
    void setClassifier(qint32 value);
    void setEnableDNN(bool state);
    void setEnableTimeMeasure(bool state);
    void setPauseVideo(bool state);


protected:
    void detectObjects(cv::Mat& frame);
    void detectObjectsDNN(cv::Mat& frame);
    void changeClassifier();
    void run() override;
    void takePhoto(cv::Mat& frame);


    QString mCameraID;
    QMutex* mDataLock;
    bool mEnableDNN;
    bool mEnableTimeMeasure;
    cv::Mat mFrame;
    bool mPauseVideo;
    bool mRunning;

    qint32 mFrameHeight;
    qint32 mFrameWidth;
    bool mTakingPhoto;
    bool mClassifierChanged;
    Classifier mClassifierType;
    std::pair<cv::dnn::Backend, cv::dnn::Target> mBackend;
    bool mBackendChanged;

    // Object analysis.
    cv::CascadeClassifier* mClassifier;

    // AI.
    cv::dnn::Net mNet;
    std::vector<std::string> mObjectClasses;


signals:
    void sFrameCaptured(cv::Mat* mat);
    void sPhotoTaken(const QString& name);


};  // class CaptureThread


}  // namespace packtpub


#endif  // packtpub_CaptureThread_h
