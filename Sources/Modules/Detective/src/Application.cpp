/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "Prerequisites.h"
#include "Application.h"
#include "MainWindow.h"

// Qt includes
#include <QCommandLineParser>
#include <QStyleFactory>
#include <QDebug>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
Application::Application(int& argc, char** argv) : QApplication(argc, argv) {
    QCoreApplication::setApplicationName("Detective");
    QCoreApplication::setApplicationVersion(tr("%1 (%2)").arg(VERSION).arg(RELEASE));
    setStyle(QStyleFactory::create("Fusion"));
}


Application::~Application() {
}


int Application::run() {
    LOG_S(INFO) << "-----------------------";
    LOG_S(INFO) << "---- START RUNNING ----";

    if(!mMainWindow.data()) {
        return -1;
    }
    mMainWindow->show();

    qint32 code = exec();

    LOG_S(INFO) << "---- STOP  RUNNING ----";
    LOG_S(INFO) << "-----------------------";
    return code;
}


void Application::shutDown() {
    mMainWindow.reset();

    LOG_S(INFO) << "**** SHUTDOWN ****";
    LOG_S(INFO) << "******************";
}


bool Application::startUp() {
    LOG_S(INFO) << "******************";
    LOG_S(INFO) << "**** START UP ****";

    mMainWindow.reset(new MainWindow());
    if(!mMainWindow->setup()) {
        LOG_S(ERROR) << "Failed to start up.";
        return false;
    }

    return true;
}


}  // namespace packtpub
