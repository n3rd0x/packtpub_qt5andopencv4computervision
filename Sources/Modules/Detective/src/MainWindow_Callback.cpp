/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QCameraInfo>
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbAppendSavePhoto(const QString& name) {
    auto cover = Utilities::getPhotoPath(name, "jpg");
    auto item  = new QStandardItem();
    mListModel->appendRow(item);

    QModelIndex index = mListModel->indexFromItem(item);
    mListModel->setData(index, QPixmap(cover).scaledToHeight(145), Qt::DecorationRole);
    mListModel->setData(index, name, Qt::DisplayRole);
    mListView->scrollTo(index);
}


void MainWindow::cbBackendSelected(qint32 idx) {
    if(idx > 0) {
        if(mCapturer) {
            auto value = mBackendList->itemData(idx).value<QPair<qint32, qint32>>();
            auto pair  = std::pair<int32_t, int32_t>(value.first, value.second);
            mCapturer->setBackend(pair);
        }
    }
}


void MainWindow::cbCameraSelected(qint32 idx) {
    if(idx > 0) {
        mCameraID->setText(mCameraList->currentData().toString());
    }
}


void MainWindow::cbClassifierSelected(qint32 idx) {
    if(idx > 0) {
        if(mCapturer) {
            mCapturer->setClassifier(idx);
        }
    }
}


void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbCloseCamera() {
    if(mCapturer) {
        mCapturer->setRunning(false);
        disconnect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
        disconnect(mCapturer, &CaptureThread::sPhotoTaken, this, &MainWindow::cbAppendSavePhoto);
        connect(mCapturer, &CaptureThread::finished, mCapturer, &CaptureThread::deleteLater);
    }
    mBtnTakePhoto->setEnabled(false);
    mChkPauseVideo->setEnabled(false);
    mChkPauseVideo->setChecked(false);
}


void MainWindow::cbOpenCamera() {
    if(mCameraID->text().isEmpty()) {
        QMessageBox::information(this, tr("Information"), "No Camera or Video File is Selected!");
        return;
    }

    cbCloseCamera();

    mCapturer = new CaptureThread(mCameraID->text(), mDataLock);
    mCapturer->setEnableDNN(mChkDnnMode->isChecked());
    mCapturer->setEnableTimeMeasure(mChkTimeMeasure->isChecked());
    mCapturer->setPauseVideo(mChkPauseVideo->isChecked());

    cbBackendSelected(mBackendList->currentIndex());
    cbClassifierSelected(mClassifierList->currentIndex());

    connect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
    connect(mCapturer, &CaptureThread::sPhotoTaken, this, &MainWindow::cbAppendSavePhoto);

    mCapturer->start();

    mMainStatus->setText(QString("Capturing Camera (%1)").arg(mCameraID->text()));
    mBtnTakePhoto->setEnabled(true);
    mChkPauseVideo->setEnabled(true);
}


void MainWindow::cbShowCameraInfo() {
    auto cameras = QCameraInfo::availableCameras();
    QString info = QString("Available Cameras:");
    for(auto itr : cameras) {
        info += "\n - " + itr.deviceName() + ": ";
        info += itr.description();
    }
    QMessageBox::information(this, tr("Cameras"), info);
}


void MainWindow::cbTakePhoto() {
    if(mCapturer) {
        mCapturer->takePhoto();
    }
}


void MainWindow::cbUpdateFrame(cv::Mat* mat) {
    {
        QMutexLocker lock(mDataLock);
        mCurrentFrame = *mat;
    }

    QImage frame(mCurrentFrame.data,
                 mCurrentFrame.cols,
                 mCurrentFrame.rows,
                 static_cast<qint32>(mCurrentFrame.step),
                 QImage::Format_RGB888);
    auto pixmap = QPixmap::fromImage(frame);

    mImageScene->clear();
    mImageView->resetMatrix();
    mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());
}


void MainWindow::cbUpdateOptions(qint32 status) {
    if(!mCapturer) {
        return;
    }

    auto box = qobject_cast<QCheckBox*>(sender());
    if(box == mChkDnnMode) {
        mCapturer->setEnableDNN(mChkDnnMode->isChecked());
    }
    else if(box == mChkTimeMeasure) {
        mCapturer->setEnableTimeMeasure(mChkTimeMeasure->isChecked());
    }
    else if(box == mChkPauseVideo) {
        mCapturer->setPauseVideo(mChkPauseVideo->isChecked());
    }
}


}  // namespace packtpub
