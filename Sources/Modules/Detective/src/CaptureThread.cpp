/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "CaptureThread.h"
#include "Utilities.h"

// Qt includes
#include <QApplication>
#include <QImage>
#include <QTime>


namespace packtpub {


// ************************************************************
// Static Implementations
// ************************************************************
void CaptureThread::decodeOutLayers(cv::Mat& frame,
                                    const std::vector<cv::Mat>& outs,
                                    std::vector<int>& outClassIds,
                                    std::vector<float>& outConfidences,
                                    std::vector<cv::Rect>& outBoxes) {
    // We can change the confidence threshold to eliminate some
    // false predictions.
    float thresholdConf = 0.5;
    float thresholdNms  = 0.4;

    std::vector<int> classIds;
    std::vector<float> confidences;
    std::vector<cv::Rect> boxes;

    for(size_t i = 0; i < outs.size(); i++) {
        float* data = reinterpret_cast<float*>(outs[i].data);
        for(auto j = 0; j < outs[i].rows; j++, data += outs[i].cols) {
            cv::Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            cv::Point classIdPoint;
            double confidence = 0.0;

            // Get the value and location of the maximun score.
            cv::minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if(confidence > thresholdConf) {
                auto centerX = static_cast<int>(data[0] * frame.cols);
                auto centerY = static_cast<int>(data[1] * frame.rows);
                auto width   = static_cast<int>(data[2] * frame.cols);
                auto height  = static_cast<int>(data[3] * frame.rows);
                auto left    = centerX - width / 2;
                auto top     = centerY - height / 2;

                classIds.push_back(classIdPoint.x);
                confidences.push_back(static_cast<float>(confidence));
                boxes.push_back(cv::Rect(left, top, width, height));
            }
        }
    }

    // Non maximum suppression.
    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confidences, thresholdConf, thresholdNms, indices);
    for(size_t i = 0; i < indices.size(); i++) {
        auto idx = indices[i];
        outClassIds.push_back(classIds[idx]);
        outBoxes.push_back(boxes[idx]);
        outConfidences.push_back(confidences[idx]);
    }
}


std::vector<std::string> CaptureThread::getOutputsName(const cv::dnn::Net& net) {
    static bool printed = false;
    static std::vector<std::string> names;
    if(names.empty()) {
        std::vector<int> outLayers           = net.getUnconnectedOutLayers();
        std::vector<std::string> layersNames = net.getLayerNames();
        names.resize(outLayers.size());

        if(!printed) {
            LOG_S(INFO) << "Print Output Names:";
        }
        for(size_t i = 0; i < outLayers.size(); i++) {
            names[i] = layersNames[outLayers[i] - 1];

            if(!printed) {
                LOG_S(INFO) << " * " << names[i];
            }
        }
        printed = true;
    }
    return names;
}



std::string CaptureThread::backendName(cv::dnn::Backend value) {
    if(value == cv::dnn::Backend::DNN_BACKEND_VKCOM) {
        return std::string("DNN_BACKEND_VKCOM");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_HALIDE) {
        return std::string("DNN_BACKEND_HALIDE");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_OPENCV) {
        return std::string("DNN_BACKEND_OPENCV");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_INFERENCE_ENGINE) {
        return std::string("DNN_BACKEND_INFERENCE_ENGINE");
    }
    return std::string("DNN_BACKEND_DEFAULT");
}


std::string CaptureThread::targetName(cv::dnn::Target value) {
    if(value == cv::dnn::Target::DNN_TARGET_FPGA) {
        return std::string("DNN_TARGET_FPGA");
    }
    if(value == cv::dnn::Target::DNN_TARGET_MYRIAD) {
        return std::string("DNN_TARGET_MYRIAD");
    }
    if(value == cv::dnn::Target::DNN_TARGET_OPENCL) {
        return std::string("DNN_TARGET_OPENCL");
    }
    if(value == cv::dnn::Target::DNN_TARGET_VULKAN) {
        return std::string("DNN_TARGET_VULKAN");
    }
    if(value == cv::dnn::Target::DNN_TARGET_OPENCL_FP16) {
        return std::string("DNN_TARGET_OPENCL_FP16");
    }
    return std::string("DNN_TARGET_CPU");
}




// ************************************************************
// Class Implementations
// ************************************************************
CaptureThread::CaptureThread(const QString& id, QMutex* lock) {
    mCameraID          = id;
    mDataLock          = lock;
    mRunning           = false;
    mTakingPhoto       = false;
    mEnableDNN         = true;
    mEnableTimeMeasure = false;
    mBackend = std::pair<cv::dnn::Backend, cv::dnn::Target>(cv::dnn::DNN_BACKEND_DEFAULT, cv::dnn::DNN_TARGET_CPU);
    mBackendChanged    = true;
    mClassifier        = nullptr;
    mClassifierType    = Classifier::FRONTAL_FACE;
    mClassifierChanged = true;

    mFrameWidth  = 0;
    mFrameHeight = 0;
}


CaptureThread::~CaptureThread() {
}


void CaptureThread::changeClassifier() {
    if(mClassifierChanged && !mEnableDNN) {
        if(mClassifier) {
            delete mClassifier;
        }
        if(mClassifierType == Classifier::BOSTON_BULL) {
            LOG_S(INFO) << "Using classifier: Boston Bull";
            mClassifier = new cv::CascadeClassifier("data/haarcascades/cascade_boston_bull.xml");
        }
        else if(mClassifierType == Classifier::FRONTAL_CAT) {
            LOG_S(INFO) << "Using classifier: Front Cat";
            mClassifier = new cv::CascadeClassifier("data/haarcascades/haarcascade_frontalcatface_extended.xml");
        }
        else {
            LOG_S(INFO) << "Using classifier: Frontal Face";
            mClassifier = new cv::CascadeClassifier("data/haarcascades/haarcascade_frontalface_default.xml");
        }
        mClassifierChanged = false;
    }
}


void CaptureThread::detectObjects(cv::Mat& frame) {
    std::vector<cv::Rect> objects;

    // 3 for no-entry-sign
    // 5 for others
    auto minNeighbors = 5;

    mClassifier->detectMultiScale(frame, objects, 1.3, minNeighbors);

    cv::Scalar colour = cv::Scalar(0, 0, 255);

    // Draw the circumscribe rectangles.
    for(size_t i = 0; i < objects.size(); i++) {
        cv::rectangle(frame, objects[i], colour, 1);
    }
}


void CaptureThread::detectObjectsDNN(cv::Mat& frame) {
    auto inW = 416;
    auto inH = 416;
    if(mNet.empty()) {
        // Pretrained models: https://modelzoo.co
        // Give the configuration and weight files for the model.
        std::string modelConfig  = "data/yolov3.cfg";
        std::string modelWeights = "data/yolov3.weights";
        mNet                     = cv::dnn::readNetFromDarknet(modelConfig, modelWeights);

        // mNet.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
        // mNet.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
        mNet.setPreferableBackend(mBackend.first);
        mNet.setPreferableTarget(mBackend.second);
        LOG_S(INFO) << "Backend: " << backendName(mBackend.first);
        LOG_S(INFO) << "Target:  " << targetName(mBackend.second);

        mObjectClasses.clear();
        std::string name;
        std::string namesFile = "data/coco.names";
        std::ifstream ifs(namesFile.c_str());
        while(getline(ifs, name)) {
            mObjectClasses.push_back(name);
        }
    }

    cv::Mat blob;
    cv::dnn::blobFromImage(frame, blob, 1 / 255.0, cv::Size(inW, inH), cv::Scalar(0, 0, 0), true, false);

    mNet.setInput(blob);

    // Forward.
    std::vector<cv::Mat> outs;
    mNet.forward(outs, getOutputsName(mNet));

    if(mEnableTimeMeasure) {
        std::vector<double> layersTime;
        auto freq = cv::getTickFrequency() * 0.001;
        auto t    = mNet.getPerfProfile(layersTime) / freq;
        DLOG_S(INFO) << "YOLO: Interence time on a single frame: " << t << "ms";
    }

    // Remove the bounding boxes with low confidence.
    std::vector<int> outClassIds;
    std::vector<float> outConfidences;
    std::vector<cv::Rect> outBoxes;
    decodeOutLayers(frame, outs, outClassIds, outConfidences, outBoxes);

    for(size_t i = 0; i < outClassIds.size(); i++) {
        cv::rectangle(frame, outBoxes[i], cv::Scalar(0, 0, 255));

        // Get the label for the class name and its cofidence.
        std::string label = mObjectClasses[outClassIds[i]];
        label += cv::format(":%.2f", outConfidences[i]);

        // Display the label at the top of the bounding box.
        int baseLine;
        cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
        int left           = outBoxes[i].x;
        int top            = outBoxes[i].y;
        top                = std::max(top, labelSize.height);
        cv::putText(frame, label, cv::Point(left, top), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));
    }
}


void CaptureThread::run() {
    LOG_S(INFO) << "---- CaptureThread START RUNNING ----";
    mRunning = true;

    QTime timer;
    qint32 camidx = -1;
    if(mCameraID.length() == 1) {
        camidx = mCameraID.toInt();
    }

    cv::VideoCapture capture;
    if(camidx >= 0) {
        capture.open(camidx);
        LOG_S(INFO) << "Open Camera (" << camidx << ").";
    }
    else {
        capture.open(mCameraID.toStdString());
        LOG_S(INFO) << "Open VideoFile (" << mCameraID << ").";
        timer.start();
    }

    capture.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    capture.set(cv::CAP_PROP_FRAME_HEIGHT, 480);


    cv::Mat capFrame;
    mFrameWidth  = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
    mFrameHeight = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));

    LOG_S(INFO) << "Resolution:   " << mFrameWidth << "x" << mFrameHeight;
    LOG_S(INFO) << "DNN Mode:     " << mEnableDNN;
    LOG_S(INFO) << "Time Measure: " << mEnableTimeMeasure;
    while(mRunning) {
        auto pause = mPauseVideo;
        if(capFrame.empty()) {
            pause = false;
        }

        if(!pause) {
            capture >> capFrame;
            if(capFrame.empty()) {
                break;
            }
        }

        // Using static image.
        //        capFrame = cv::imread("data/bosten_bull.jpg");

        // Change classifier or DNN.
        changeClassifier();
        if(mBackendChanged) {
            if(!mNet.empty()) {
                mNet = cv::dnn::Net();
            }
            mBackendChanged = false;
        }

        if(mTakingPhoto) {
            takePhoto(capFrame);
        }

        qint64 start = 0;
        if(mEnableTimeMeasure) {
            start = cv::getTickCount();
        }

        if(mEnableDNN) {
            detectObjectsDNN(capFrame);
        }
        else {
            detectObjects(capFrame);
        }

        if(mEnableTimeMeasure) {
            qint64 end = cv::getTickCount();
            qreal t    = (end - start) * 1000.0 / cv::getTickFrequency();
            DLOG_S(INFO) << "Detection time on a single frame: " << t << "ms";
        }

        if(!pause) {
            cvtColor(capFrame, capFrame, cv::COLOR_BGR2RGB);
            {
                QMutexLocker lock(mDataLock);
                mFrame = capFrame;
            }
        }
        Q_EMIT sFrameCaptured(&mFrame);

        // Constrains to 25 FPS if open a video file.
        if(camidx < 0) {
            // Sec / Fps * ms
            // (1 / 25) * 1000
            auto time   = 40;
            auto elapse = timer.elapsed();
            if(elapse < time) {
                time = time - elapse;
            }
            else {
                time = 0;
            }
            if(pause) {
                time = 40;
            }
            msleep(time);
            timer.restart();
        }
    }

    capture.release();

    if(mClassifier) {
        delete mClassifier;
        mClassifier = nullptr;
    }

    mRunning = false;
    LOG_S(INFO) << "---- CaptureThread  STOP RUNNING ----";
}


void CaptureThread::setBackend(std::pair<int32_t, int32_t> value) {
    auto pair = std::pair<cv::dnn::Backend, cv::dnn::Target>(static_cast<cv::dnn::Backend>(value.first),
                                                             static_cast<cv::dnn::Target>(value.second));
    if(mBackend != pair) {
        mBackend        = pair;
        mBackendChanged = true;
    }
}


void CaptureThread::setClassifier(qint32 value) {
    if(value < 1) {
        return;
    }
    auto type = static_cast<Classifier>(value);
    if(mClassifierType != type) {
        mClassifierType    = type;
        mClassifierChanged = true;
    }
}


void CaptureThread::setEnableDNN(bool state) {
    mEnableDNN = state;
}


void CaptureThread::setEnableTimeMeasure(bool state) {
    mEnableTimeMeasure = state;
}


void CaptureThread::setRunning(const bool state) {
    mRunning = state;
}


void CaptureThread::setPauseVideo(bool state) {
    mPauseVideo = state;
}


void CaptureThread::takePhoto() {
    mTakingPhoto = true;
}


void CaptureThread::takePhoto(cv::Mat& frame) {
    auto name = Utilities::newPhotoName();
    auto path = Utilities::getPhotoPath(name, "jpg");

    cv::imwrite(path.toStdString(), frame);
    Q_EMIT sPhotoTaken(name);
    mTakingPhoto = false;
}


}  // namespace packtpub
