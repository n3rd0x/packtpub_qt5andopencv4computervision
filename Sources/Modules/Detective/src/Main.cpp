/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "Application.h"


// ************************************************************
// Main
// ************************************************************
int main(int argc, char** argv) {
    // Initialise the logger.
    loguru::init(argc, argv);
    loguru::add_file("06_Detective.log", loguru::Truncate, loguru::Verbosity_MAX);

    // Setup and run.
    packtpub::Application app(argc, argv);
    qint32 exec = 0;
    if(app.startUp()) {
        exec = app.run();
    }

    app.shutDown();
    return exec;
}
