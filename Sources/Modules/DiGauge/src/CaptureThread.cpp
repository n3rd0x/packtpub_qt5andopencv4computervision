/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "CaptureThread.h"
#include "Utilities.h"

// Qt includes
#include <QApplication>
#include <QImage>
#include <QTime>


namespace packtpub {


// ************************************************************
// Static Implementations
// ************************************************************
void CaptureThread::decodeOutLayers(cv::Mat& frame, const std::vector<cv::Mat>& outs, std::vector<cv::Rect>& outBoxes) {
    // We can change the confidence threshold to eliminate some
    // false predictions.
    float thresholdConf = 0.65f;  // confidence threshold
    float thresholdNms  = 0.4f;   // non-maximu suppression threshold

    std::vector<float> confidences;
    std::vector<cv::Rect> boxes;

    for(size_t i = 0; i < outs.size(); i++) {
        float* data = reinterpret_cast<float*>(outs[i].data);
        for(auto j = 0; j < outs[i].rows; j++, data += outs[i].cols) {
            cv::Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            cv::Point classIdPoint;
            double confidence = 0.0;

            // Get the value and location of the maximun score.
            cv::minMaxLoc(scores, nullptr, &confidence, nullptr, &classIdPoint);

            // Not a car.
            if(classIdPoint.x != 2) {
                continue;
            }

            if(confidence > static_cast<double>(thresholdConf)) {
                auto centerX = static_cast<int>(data[0] * frame.cols);
                auto centerY = static_cast<int>(data[1] * frame.rows);
                auto width   = static_cast<int>(data[2] * frame.cols);
                auto height  = static_cast<int>(data[3] * frame.rows);
                auto left    = centerX - width / 2;
                auto top     = centerY - height / 2;

                confidences.push_back(static_cast<float>(confidence));
                boxes.push_back(cv::Rect(left, top, width, height));
            }
        }
    }

    // Non maximum suppression.
    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confidences, thresholdConf, thresholdNms, indices);
    for(size_t i = 0; i < indices.size(); i++) {
        auto idx = indices[i];
        outBoxes.push_back(boxes[idx]);
    }
}


std::string CaptureThread::backendName(cv::dnn::Backend value) {
    if(value == cv::dnn::Backend::DNN_BACKEND_VKCOM) {
        return std::string("DNN_BACKEND_VKCOM");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_HALIDE) {
        return std::string("DNN_BACKEND_HALIDE");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_OPENCV) {
        return std::string("DNN_BACKEND_OPENCV");
    }
    if(value == cv::dnn::Backend::DNN_BACKEND_INFERENCE_ENGINE) {
        return std::string("DNN_BACKEND_INFERENCE_ENGINE");
    }
    return std::string("DNN_BACKEND_DEFAULT");
}


std::string CaptureThread::targetName(cv::dnn::Target value) {
    if(value == cv::dnn::Target::DNN_TARGET_FPGA) {
        return std::string("DNN_TARGET_FPGA");
    }
    if(value == cv::dnn::Target::DNN_TARGET_MYRIAD) {
        return std::string("DNN_TARGET_MYRIAD");
    }
    if(value == cv::dnn::Target::DNN_TARGET_OPENCL) {
        return std::string("DNN_TARGET_OPENCL");
    }
    if(value == cv::dnn::Target::DNN_TARGET_VULKAN) {
        return std::string("DNN_TARGET_VULKAN");
    }
    if(value == cv::dnn::Target::DNN_TARGET_OPENCL_FP16) {
        return std::string("DNN_TARGET_OPENCL_FP16");
    }
    return std::string("DNN_TARGET_CPU");
}




// ************************************************************
// Class Implementations
// ************************************************************
CaptureThread::CaptureThread(const QString& id, QMutex* lock) {
    mCameraID          = id;
    mDataLock          = lock;
    mRunning           = false;
    mTakingPhoto       = false;
    mEnableTimeMeasure = false;
    mBackend = std::pair<cv::dnn::Backend, cv::dnn::Target>(cv::dnn::DNN_BACKEND_DEFAULT, cv::dnn::DNN_TARGET_CPU);
    mBackendChanged = true;
    mFrameWidth     = 0;
    mFrameHeight    = 0;
    mViewMode       = ViewMode::BIRD_EYE;

    // Average car length.
    mCarLength = 5.0;
}


CaptureThread::~CaptureThread() {
}


void CaptureThread::detectObjectsDNN(cv::Mat& frame) {
    auto inW = 416;
    auto inH = 416;
    if(mNet.empty()) {
        // Pretrained models: https://modelzoo.co
        // Give the configuration and weight files for the model.
        std::string modelConfig  = "data/yolov3.cfg";
        std::string modelWeights = "data/yolov3.weights";
        mNet                     = cv::dnn::readNetFromDarknet(modelConfig, modelWeights);

        // mNet.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
        // mNet.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
        mNet.setPreferableBackend(mBackend.first);
        mNet.setPreferableTarget(mBackend.second);
        LOG_S(INFO) << "Backend: " << backendName(mBackend.first);
        LOG_S(INFO) << "Target:  " << targetName(mBackend.second);

        mObjectClasses.clear();
        std::string name;
        std::string namesFile = "data/coco.names";
        std::ifstream ifs(namesFile.c_str());
        while(getline(ifs, name)) {
            mObjectClasses.push_back(name);
        }
    }

    cv::Mat blob;
    cv::dnn::blobFromImage(frame, blob, 1 / 255.0, cv::Size(inW, inH), cv::Scalar(0, 0, 0), true, false);

    mNet.setInput(blob);

    // Forward.
    std::vector<cv::Mat> outs;
    mNet.forward(outs, mNet.getUnconnectedOutLayersNames());

    if(mEnableTimeMeasure) {
        std::vector<double> layersTime;
        auto freq = cv::getTickFrequency() * 0.001;
        auto t    = mNet.getPerfProfile(layersTime) / freq;
        DLOG_S(INFO) << "YOLO: Interence time on a single frame: " << t << "ms";
    }

    // Remove the bounding boxes with low confidence.
    std::vector<cv::Rect> outBoxes;
    decodeOutLayers(frame, outs, outBoxes);

    for(size_t i = 0; i < outBoxes.size(); i++) {
        cv::rectangle(frame, outBoxes[i], cv::Scalar(0, 0, 255));
    }

    if(mViewMode == ViewMode::BIRD_EYE) {
        distanceBirdEye(frame, outBoxes);
    }
    else {
        distanceEyeLevel(frame, outBoxes);
    }
}


void CaptureThread::distanceBirdEye(cv::Mat& frame, std::vector<cv::Rect>& cars) {
    if(cars.empty()) {
        return;
    }

    std::vector<int32_t> lengthOfCars;
    std::vector<std::pair<int32_t, int32_t>> endpoints;
    std::vector<std::pair<int32_t, int32_t>> carsMerged;

    for(auto itr : cars) {
        lengthOfCars.push_back(itr.width);
        endpoints.push_back(std::make_pair(itr.x, 1));               // Back end of the car
        endpoints.push_back(std::make_pair(itr.x + itr.width, -1));  // Front end of the car
    }

    std::sort(lengthOfCars.begin(), lengthOfCars.end());
    auto length = lengthOfCars[cars.size() / 2];
    std::sort(endpoints.begin(), endpoints.end(), [](std::pair<int32_t, int32_t> a, std::pair<int32_t, int32_t> b) {
        return a.first < b.first;
    });

    int32_t flag  = 0;
    int32_t start = 0;
    for(auto itr : endpoints) {
        flag += itr.second;
        if(flag == 1 && start == 0) {  // Start
            start = itr.first;
        }
        else if(flag == 0) {  // End
            carsMerged.push_back(std::make_pair(start, itr.first));
            start = 0;
        }
    }

    for(size_t i = 1; i < carsMerged.size(); i++) {
        // Head of car, start of spacing.
        auto x1 = carsMerged[i - 1].second;

        // End of another car, end of spacing.
        auto x2 = carsMerged[i].first;

        // First line (end of 1st car).
        cv::line(frame, cv::Point(x1, 0), cv::Point(x1, frame.rows), cv::Scalar(0, 255, 0), 2);

        // Second line (head of 2nd car).
        cv::line(frame, cv::Point(x2, 0), cv::Point(x2, frame.rows), cv::Scalar(0, 0, 255), 2);
        float distance = (x2 - x1) * (mCarLength / length);

        // Display the label at the top the bounding box.
        int32_t baseline;
        std::string label = cv::format("%.2f m", distance);
        cv::Size size     = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseline);
        int32_t posx      = (x1 + x2) / 2 - (size.width / 2);
        cv::rectangle(frame, cv::Rect(posx, 2, size.width, size.height * 2.0), cv::Scalar(255, 255, 255), -1);
        cv::putText(frame, label, cv::Point(posx, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0));
    }
}


void CaptureThread::distanceEyeLevel(cv::Mat& frame, std::vector<cv::Rect>& cars) {
    const float d0 = 1000.0f;  // cm
    const float w0 = 150.0f;   // px

    // Find the target car: the most middle and biggest one.
    std::vector<cv::Rect> cars_in_middle;
    std::vector<int32_t> cars_area;
    size_t target_idx = 0;

    for(auto itr : cars) {
        if(itr.x < frame.cols / 2 && (itr.x + itr.width) > frame.cols / 2) {
            cars_in_middle.push_back(itr);
            auto area = itr.width * itr.height;
            cars_area.push_back(area);
            if(area > cars_area[target_idx]) {
                target_idx = cars_area.size() - 1;
            }
        }
    }

    if(cars_in_middle.size() <= target_idx) {
        return;
    }

    cv::Rect car  = cars_in_middle[target_idx];
    auto distance = (w0 / car.width) * d0;  // (w0 / w1) * d0

    // Display the label at the top-left corner of the bounding box.
    auto label    = cv::format("%.2f m", distance / 100.0);
    auto baseline = 0;
    auto size     = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseline);
    cv::rectangle(
        frame, cv::Rect(car.x + 2.0, car.y + 2.0, size.width, size.height * 1.5), cv::Scalar(255, 255, 255), -1);
    cv::putText(frame,
                label,
                cv::Point(car.x, car.y + 2.0 + size.height),
                cv::FONT_HERSHEY_SIMPLEX,
                0.5,
                cv::Scalar(255, 0, 0));
}


void CaptureThread::run() {
    LOG_S(INFO) << "---- CaptureThread START RUNNING ----";
    mRunning = true;

    QTime timer;
    qint32 camidx = -1;
    if(mCameraID.length() == 1) {
        camidx = mCameraID.toInt();
    }

    cv::VideoCapture capture;
    if(camidx >= 0) {
        capture.open(camidx);
        LOG_S(INFO) << "Open Camera (" << camidx << ").";
    }
    else {
        capture.open(mCameraID.toStdString());
        LOG_S(INFO) << "Open VideoFile (" << mCameraID << ").";
        timer.start();
    }

    capture.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    capture.set(cv::CAP_PROP_FRAME_HEIGHT, 480);


    cv::Mat capFrame;
    mFrameWidth  = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
    mFrameHeight = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));

    LOG_S(INFO) << "Resolution:   " << mFrameWidth << "x" << mFrameHeight;
    LOG_S(INFO) << "Time Measure: " << mEnableTimeMeasure;
    while(mRunning) {
        auto pause = mPauseVideo;
        if(capFrame.empty()) {
            pause = false;
        }

        if(!pause) {
            capture >> capFrame;
            if(capFrame.empty()) {
                break;
            }
        }

        if(mTakingPhoto) {
            takePhoto(capFrame);
        }

        qint64 start = 0;
        if(mEnableTimeMeasure) {
            start = cv::getTickCount();
        }

        detectObjectsDNN(capFrame);

        if(mEnableTimeMeasure) {
            qint64 end = cv::getTickCount();
            qreal t    = (end - start) * 1000.0 / cv::getTickFrequency();
            DLOG_S(INFO) << "Detection time on a single frame: " << t << "ms";
        }

        if(!pause) {
            cvtColor(capFrame, capFrame, cv::COLOR_BGR2RGB);
            {
                QMutexLocker lock(mDataLock);
                mFrame = capFrame;
            }
        }
        Q_EMIT sFrameCaptured(&mFrame);

        // Constrains to 25 FPS if open a video file.
        if(camidx < 0) {
            // Sec / Fps * ms
            // (1 / 25) * 1000
            auto time   = 40;
            auto elapse = timer.elapsed();
            if(elapse < time) {
                time = time - elapse;
            }
            else {
                time = 0;
            }
            if(pause) {
                time = 40;
            }
            msleep(time);
            timer.restart();
        }
    }

    capture.release();
    mRunning = false;
    LOG_S(INFO) << "---- CaptureThread  STOP RUNNING ----";
}


void CaptureThread::setBackend(std::pair<int32_t, int32_t> value) {
    auto pair = std::pair<cv::dnn::Backend, cv::dnn::Target>(static_cast<cv::dnn::Backend>(value.first),
                                                             static_cast<cv::dnn::Target>(value.second));
    if(mBackend != pair) {
        mBackend        = pair;
        mBackendChanged = true;
    }
}


void CaptureThread::setCarLength(double value) {
    mCarLength = value;
}


void CaptureThread::setEnableTimeMeasure(bool state) {
    mEnableTimeMeasure = state;
}


void CaptureThread::setRunning(const bool state) {
    mRunning = state;
}


void CaptureThread::setPauseVideo(bool state) {
    mPauseVideo = state;
}


void CaptureThread::setViewMode(ViewMode mode) {
    mViewMode = mode;
}


void CaptureThread::takePhoto() {
    mTakingPhoto = true;
}


void CaptureThread::takePhoto(cv::Mat& frame) {
    auto name = Utilities::newPhotoName();
    auto path = Utilities::getPhotoPath(name, "jpg");

    cv::imwrite(path.toStdString(), frame);
    Q_EMIT sPhotoTaken(name);
    mTakingPhoto = false;
}


}  // namespace packtpub
