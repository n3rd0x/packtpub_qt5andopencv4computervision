/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QPair>
#include <QCameraInfo>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
    cbCloseCamera();
}


void MainWindow::initPointers() {
    mImageScene = nullptr;
    mImageView  = nullptr;
    mMainStatus = nullptr;
    mCapturer   = nullptr;
    mDataLock   = nullptr;
}


void MainWindow::initValues() {
}


void MainWindow::populateBackendAndTarget() {
    auto backends = cv::dnn::getAvailableBackends();

    mBackendList->addItem("** Select Backend **",
                          QVariant::fromValue<QPair<qint32, qint32>>(QPair<qint32, qint32>(
                              cv::dnn::Backend::DNN_BACKEND_DEFAULT, cv::dnn::Target::DNN_TARGET_CPU)));
    for(auto itr : backends) {
        auto backend = CaptureThread::backendName(itr.first);
        auto target  = CaptureThread::targetName(itr.second);
        auto name    = backend + " (" + target + ")";
        mBackendList->addItem(QString::fromStdString(name),
                              QVariant::fromValue<QPair<qint32, qint32>>(QPair<qint32, qint32>(itr.first, itr.second)));
    }
}


void MainWindow::populateSaveList() {
    QDir dir(Utilities::getDataPath());
    QStringList nameFilters;
    nameFilters << "*.jpg";

    QFileInfoList files = dir.entryInfoList(nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);
    for(auto cover : files) {
        auto name = cover.baseName();
        auto item = new QStandardItem();
        mListModel->appendRow(item);

        auto index = mListModel->indexFromItem(item);
        mListModel->setData(index, QPixmap(cover.absoluteFilePath()).scaledToHeight(145), Qt::DecorationRole);
        mListModel->setData(index, name, Qt::DisplayRole);
    }
}


void MainWindow::populateTestData() {
    QDir dir("data/testdata");
    QStringList nameFilters;
    nameFilters << "*.mp4"
                << "*.avi"
                << "*.ppm"
                << "*.jpg"
                << "*.jpeg";

    QFileInfoList files = dir.entryInfoList(nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);
    for(auto file : files) {
        mCameraList->addItem(file.fileName(), file.absoluteFilePath());
    }
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mActionExit, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mActionCamInfo, &QAction::triggered, this, &MainWindow::cbShowCameraInfo);
    connect(mActionOpenCam, &QAction::triggered, this, &MainWindow::cbOpenCamera);
    connect(mActionBirdEye, &QAction::triggered, this, &MainWindow::cbChangeViewModel);
    connect(mActionEyeLevel, &QAction::triggered, this, &MainWindow::cbChangeViewModel);
    connect(mBtnOpenCamera, &QPushButton::clicked, this, &MainWindow::cbOpenCamera);
    connect(mRdHorizontal, &QRadioButton::toggled, this, &MainWindow::cbCarLengthSwitch);
    connect(mRdVertical, &QRadioButton::toggled, this, &MainWindow::cbCarLengthSwitch);
    connect(mCameraList,
            static_cast<void (QComboBox::*)(qint32)>(&QComboBox::currentIndexChanged),
            this,
            &MainWindow::cbCameraSelected);
    connect(mChkTimeMeasure,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateOptions);
    connect(mChkPauseVideo,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateOptions);
    connect(mCarLength,
            static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this,
            &MainWindow::cbCarLengthChanged);
    return true;
}


bool MainWindow::setupDefaultSettings() {
    mActionBirdEye->setChecked(true);
    return true;
}


bool MainWindow::setupFinalState() {
    resize(1024, 756);
    setWindowTitle(qApp->applicationName());
    populateSaveList();
    populateTestData();
    populateBackendAndTarget();
    return true;
}


bool MainWindow::setupUis() {
    // Setup UI defined in the designer.
    setupUi(this);

    auto layout = new QGridLayout(mDisplay);
    mImageScene = new QGraphicsScene(this);
    mImageView  = new QGraphicsView(mImageScene);
    layout->addWidget(mImageView, 0, 0, 1, 1);

    // List cameras.
    mCameraList->addItem("** Select Camera **", QString(""));
    auto cameras = QCameraInfo::availableCameras();
    auto idx     = 0;
    for(auto itr : cameras) {
        mCameraList->addItem(itr.description(), QString::number(idx++));
    }

    // List view.
    mListModel = new QStandardItemModel(this);
    mListView->setModel(mListModel);

    // Setup status bar.
    mMainStatus = new QLabel(mStatusBar);
    mStatusBar->addPermanentWidget(mMainStatus);
    mMainStatus->setText(tr("DiGauge is Ready!"));
    return true;
}


}  // namespace packtpub
