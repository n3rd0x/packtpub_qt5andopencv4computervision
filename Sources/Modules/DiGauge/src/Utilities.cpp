/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Utilities.h"

// Qt includes
#include <QApplication>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
QString Utilities::getDataPath() {
    QString path = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation)[0];
    QDir dir(path);
    dir.mkpath("DiGauge");
    return dir.absoluteFilePath("DiGauge");
}


QString Utilities::getPhotoPath(const QString& name, const QString& postfix) {
    return QString("%1/%2.%3").arg(getDataPath(), name, postfix);
}


QString Utilities::newPhotoName() {
    QDateTime time = QDateTime::currentDateTime();
    return time.toString("yyyyMMdd_HHmmss");
}


}  // namespace packtpub
