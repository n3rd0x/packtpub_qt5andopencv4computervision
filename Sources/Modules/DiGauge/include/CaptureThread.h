/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_CaptureThread_h
#define packtpub_CaptureThread_h


// Qt includes
#include <QMutex>
#include <QString>
#include <QThread>

// OpenCV includes
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/dnn.hpp>

// STL includes
#include <vector>


namespace packtpub {


/**
 * @brief Capture threading.
 */
class CaptureThread : public QThread {
    Q_OBJECT
public:
    enum class Classifier : qint32 { FRONTAL_FACE, FRONTAL_CAT, BOSTON_BULL };
    enum class ViewMode : qint32 { BIRD_EYE, EYE_LEVEL };
    CaptureThread(const QString& id, QMutex* lock);
    ~CaptureThread() override;
    void setRunning(const bool state);
    void takePhoto();

    static void decodeOutLayers(cv::Mat& frame, const std::vector<cv::Mat>& outs, std::vector<cv::Rect>& outBoxes);
    static std::string backendName(cv::dnn::Backend value);
    static std::string targetName(cv::dnn::Target value);


public slots:
    void setBackend(std::pair<int32_t, int32_t> value);
    void setCarLength(double value);
    void setEnableTimeMeasure(bool state);
    void setPauseVideo(bool state);
    void setViewMode(ViewMode value);


protected:
    void detectObjectsDNN(cv::Mat& frame);
    void distanceBirdEye(cv::Mat& frame, std::vector<cv::Rect>& cars);
    void distanceEyeLevel(cv::Mat& frame, std::vector<cv::Rect>& cars);
    void run() override;
    void takePhoto(cv::Mat& frame);


    QString mCameraID;
    QMutex* mDataLock;
    bool mEnableTimeMeasure;
    cv::Mat mFrame;
    bool mPauseVideo;
    bool mRunning;
    double mCarLength;

    qint32 mFrameHeight;
    qint32 mFrameWidth;
    bool mTakingPhoto;
    std::pair<cv::dnn::Backend, cv::dnn::Target> mBackend;
    bool mBackendChanged;
    ViewMode mViewMode;

    // AI.
    cv::dnn::Net mNet;
    std::vector<std::string> mObjectClasses;


signals:
    void sFrameCaptured(cv::Mat* mat);
    void sPhotoTaken(const QString& name);


};  // class CaptureThread


}  // namespace packtpub


#endif  // packtpub_CaptureThread_h
