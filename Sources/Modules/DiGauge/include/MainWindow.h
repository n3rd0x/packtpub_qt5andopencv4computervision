/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



#ifndef packtpub_MainWindow_h
#define packtpub_MainWindow_h


// Local includes
#include "CaptureThread.h"

// Qt includes
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QMainWindow>
#include <QStandardItem>

#include <ui_MainWindow.h>

// OpenCV includes
#include <opencv2/opencv.hpp>


namespace packtpub {


/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow, protected Ui::MainWindow {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Default constructor.
     * @param parent Parent of this window.
     */
    explicit MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Default destructor.
     */
    virtual ~MainWindow();


    /**
     * @brief Setup the window.
     * @return True on success.
     */
    bool setup();



protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Initialise pointers.
     */
    void initPointers();


    /**
     * @brief Initialise values.
     */
    void initValues();


    /**
     * @brief Populate save list.
     */
    void populateBackendAndTarget();
    void populateSaveList();
    void populateTestData();


    /**
     * @brief Setup connections.
     * @return True on success.
     */
    bool setupConnections();


    /**
     * @brief Set default settings.
     * @return True on success.
     */
    bool setupDefaultSettings();


    /**
     * @brief Finalise the setup.
     * @return True on success.
     */
    bool setupFinalState();


    /**
     * @brief Setup UIs.
     * @return True on success.
     */
    bool setupUis();


    /**
     * @brief References.
     */
    QGraphicsScene* mImageScene;
    QGraphicsView* mImageView;
    QLabel* mMainStatus;
    QStandardItemModel* mListModel;

    cv::Mat mCurrentFrame;

    // Capture thread.
    QMutex* mDataLock;
    CaptureThread* mCapturer;


protected slots:
    void cbAppendSavePhoto(const QString& name);
    void cbExit();
    void cbBackendSelected(qint32 idx);
    void cbCameraSelected(qint32 idx);
    void cbCarLengthChanged(double value);
    void cbCarLengthSwitch();
    void cbChangeViewModel();
    void cbCloseCamera();
    void cbOpenCamera();
    void cbShowCameraInfo();
    void cbTakePhoto();
    void cbUpdateFrame(cv::Mat* mat);
    void cbUpdateOptions(qint32 status);


};  // class MainWindow


}  // namespace packtpub


#endif  // packtpub_MainWindow_h
