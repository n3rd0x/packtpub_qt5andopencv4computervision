/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "Prerequisites.h"
#include "MainWindow.h"

// Qt includes
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
}


void MainWindow::initPointers() {
    mFileMenu    = nullptr;
    mEditMenu    = nullptr;
    mViewMenu    = nullptr;
    mFileToolBar = nullptr;
    mEditToolBar = nullptr;
    mViewToolBar = nullptr;
    mImageScene  = nullptr;
    mImageView   = nullptr;
    mStatusBar   = nullptr;
    mMainStatus  = nullptr;

    mOpenAction   = nullptr;
    mSaveAsAction = nullptr;
    mExitAction   = nullptr;

    mZoomInAction  = nullptr;
    mZoomOutAction = nullptr;
    mPrevAction    = nullptr;
    mNextAction    = nullptr;
    mCurrentImage  = nullptr;

    mBlurAction = nullptr;
}


void MainWindow::initValues() {
}


void MainWindow::loadPlugins() {
    QDir pluginDirs(qApp->applicationDirPath() + "/plugins");
    QStringList nameFilters;
    nameFilters << "*.so"
                << "*.dylib"
                << "*.dll";

    QFileInfoList plugins = pluginDirs.entryInfoList(nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);
    for(auto itr : plugins) {
        QPluginLoader pluginLoader(itr.absoluteFilePath(), this);
        auto pluginPtr = qobject_cast<PluginInterface*>(pluginLoader.instance());
        if(pluginPtr) {
            auto action = new QAction(pluginPtr->name());
            mEditMenu->addAction(action);
            mEditToolBar->addAction(action);
            mPlugins[pluginPtr->name()] = pluginPtr;
            connect(action, &QAction::triggered, this, &MainWindow::cbPreformPlugin);
        }
        else {
            LOG_S(WARNING) << "Bad plugin: " << itr.absoluteFilePath();
        }
    }
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupActions()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupShortcuts()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupActions() {
    // Menu actions.
    mOpenAction = new QAction("&Open", this);
    mFileMenu->addAction(mOpenAction);
    mSaveAsAction = new QAction("&Save As", this);
    mFileMenu->addAction(mSaveAsAction);
    mExitAction = new QAction("E&xit", this);
    mFileMenu->addAction(mExitAction);

    mZoomInAction = new QAction("Zoom In", this);
    mViewMenu->addAction(mZoomInAction);
    mZoomOutAction = new QAction("Zoom Out", this);
    mViewMenu->addAction(mZoomOutAction);
    mPrevAction = new QAction("&Previous Image", this);
    mViewMenu->addAction(mPrevAction);
    mNextAction = new QAction("&Next Image", this);
    mViewMenu->addAction(mNextAction);

    mBlurAction = new QAction("Blur", this);
    mEditMenu->addAction(mBlurAction);

    // Toolbar actions.
    mFileToolBar->addAction(mOpenAction);

    mViewToolBar->addAction(mZoomInAction);
    mViewToolBar->addAction(mZoomOutAction);
    mViewToolBar->addAction(mPrevAction);
    mViewToolBar->addAction(mNextAction);

    mEditToolBar->addAction(mBlurAction);

    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mExitAction, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mOpenAction, &QAction::triggered, this, &MainWindow::cbOpenImage);
    connect(mSaveAsAction, &QAction::triggered, this, &MainWindow::cbSaveAs);
    connect(mZoomInAction, &QAction::triggered, this, &MainWindow::cbZoomIn);
    connect(mZoomOutAction, &QAction::triggered, this, &MainWindow::cbZoomOut);
    connect(mPrevAction, &QAction::triggered, this, &MainWindow::cbPrevImage);
    connect(mNextAction, &QAction::triggered, this, &MainWindow::cbNextImage);
    connect(mBlurAction, &QAction::triggered, this, &MainWindow::cbBlurImage);

    return true;
}


bool MainWindow::setupDefaultSettings() {
    return true;
}


bool MainWindow::setupFinalState() {
    resize(800, 600);
    setWindowTitle(qApp->applicationName());
    loadPlugins();
    return true;
}


bool MainWindow::setupShortcuts() {
    QList<QKeySequence> shortcuts;
    shortcuts << Qt::Key_Plus << Qt::Key_Equal;
    mZoomInAction->setShortcuts(shortcuts);

    shortcuts.clear();
    shortcuts << Qt::Key_Minus << Qt::Key_Underscore;
    mZoomOutAction->setShortcuts(shortcuts);

    shortcuts.clear();
    shortcuts << Qt::Key_Up << Qt::Key_Left;
    mPrevAction->setShortcuts(shortcuts);

    shortcuts.clear();
    shortcuts << Qt::Key_Down << Qt::Key_Right;
    mNextAction->setShortcuts(shortcuts);
    return true;
}


bool MainWindow::setupUis() {
    // Setup menubar.
    mFileMenu = menuBar()->addMenu("&File");
    mViewMenu = menuBar()->addMenu("&View");
    mEditMenu = menuBar()->addMenu("&Edit");

    // Setup toolbar.
    mFileToolBar = addToolBar("File");
    mViewToolBar = addToolBar("View");
    mEditToolBar = addToolBar("Edit");

    // Main area for image display.
    mImageScene = new QGraphicsScene(this);
    mImageView  = new QGraphicsView(mImageScene);
    setCentralWidget(mImageView);

    // Setup status bar.
    mStatusBar  = statusBar();
    mMainStatus = new QLabel(mStatusBar);
    mStatusBar->addPermanentWidget(mMainStatus);
    mMainStatus->setText(tr("Image Information Will Be Here!"));
    return true;
}


void MainWindow::showImage(QString path) {
    mImageScene->clear();
    mImageView->resetMatrix();

    QPixmap image(path);
    mCurrentImage = mImageScene->addPixmap(image);
    mImageScene->update();
    mImageView->setSceneRect(image.rect());

    mMainStatus->setText(
        QString("%1, %2x%3, %4 Bytes").arg(path).arg(image.width()).arg(image.height()).arg(QFile(path).size()));
    mCurrentPath = path;
}


void MainWindow::toogleImage(const bool nextAction) {
    QFileInfo current(mCurrentPath);
    QDir dir = current.absoluteDir();

    QStringList nameFilters;
    nameFilters << "*.png"
                << "*.bmp"
                << "*.jpg";

    QStringList fileNames = dir.entryList(nameFilters, QDir::Files, QDir::Name);
    qint32 idx            = fileNames.indexOf(QRegExp(QRegExp::escape((current.fileName()))));
    if(!nextAction) {
        if(idx > 0) {
            showImage(dir.absoluteFilePath(fileNames.at(idx - 1)));
        }
        else {
            QMessageBox::information(this, tr("Information"), tr("Current image is the first one."));
        }
    }
    else {
        if(idx < fileNames.size() - 1) {
            showImage(dir.absoluteFilePath(fileNames.at(idx + 1)));
        }
        else {
            QMessageBox::information(this, tr("Information"), tr("Current image is the last one."));
        }
    }
}


}  // namespace packtpub
