/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"

// Qt includes
#include <QFileDialog>
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>

// OpenCV includes
#include <opencv2/opencv.hpp>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbBlurImage() {
    if(mCurrentImage == nullptr) {
        QMessageBox::information(this, tr("Information"), tr("No image to edit."));
        return;
    }

    QPixmap pixmap = mCurrentImage->pixmap();
    QImage image   = pixmap.toImage();
    image          = image.convertToFormat(QImage::Format_RGB888);
    cv::Mat mat
        = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), static_cast<size_t>(image.bytesPerLine()));

    cv::Mat tmp;
    cv::blur(mat, tmp, cv::Size(8, 8));
    mat = tmp;

    QImage imgBlurred(mat.data, mat.cols, mat.rows, static_cast<qint32>(mat.step), QImage::Format_RGB888);

    pixmap = QPixmap::fromImage(imgBlurred);
    mImageScene->clear();
    mImageView->resetMatrix();

    mCurrentImage = mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());

    QString status = QString("Image Editted (Blurred), %1x%2").arg(pixmap.width()).arg(pixmap.height());
    mMainStatus->setText(status);
}


void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbOpenImage() {
    QFileDialog dialog(this);
    dialog.setWindowTitle(tr("Open Image"));
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));

    QStringList filePaths;
    if(dialog.exec()) {
        filePaths = dialog.selectedFiles();
        showImage(filePaths.at(0));
    }
}


void MainWindow::cbNextImage() {
    toogleImage(true);
}


void MainWindow::cbPrevImage() {
    toogleImage(false);
}


void MainWindow::cbPreformPlugin() {
    if(mCurrentImage == nullptr) {
        QMessageBox::information(this, tr("Information"), tr("No image to edit."));
        return;
    }

    auto action = qobject_cast<QAction*>(sender());
    auto plugin = mPlugins[action->text()];
    if(!plugin) {
        QMessageBox::information(this, tr("Information"), "No plugin is found.");
        return;
    }

    auto pixmap = mCurrentImage->pixmap();
    auto image  = pixmap.toImage();
    image       = image.convertToFormat(QImage::Format_RGB888);
    auto mat    = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), image.bytesPerLine());

    plugin->edit(mat, mat);

    QImage edited(mat.data, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
    pixmap = QPixmap::fromImage(edited);
    mImageScene->clear();
    mImageView->resetMatrix();
    mCurrentImage = mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());

    QString status = QString("Edited Image (%1), %2x%3").arg(action->text()).arg(pixmap.width()).arg(pixmap.height());
    mMainStatus->setText(status);
}


void MainWindow::cbSaveAs() {
    if(mCurrentImage == nullptr) {
        QMessageBox::information(this, tr("Information"), tr("Nothing to save."));
        return;
    }

    QFileDialog dialog(this);
    dialog.setWindowTitle(tr("Save Image As ..."));
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));

    QStringList fileNames;
    if(dialog.exec()) {
        fileNames = dialog.selectedFiles();
        if(QRegExp(".+\\.(png|bmp|jpg)").exactMatch(fileNames.at(0))) {
            mCurrentImage->pixmap().save(fileNames.at(0));
        }
        else {
            QMessageBox::information(this, tr("Information"), tr("Save error: bad format or filename."));
        }
    }
}


void MainWindow::cbZoomIn() {
    mImageView->scale(1.2, 1.2);
}


void MainWindow::cbZoomOut() {
    mImageView->scale(1.0 / 1.2, 1.0 / 1.2);
}


}  // namespace packtpub
