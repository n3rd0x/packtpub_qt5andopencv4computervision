/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



#ifndef packtpub_MainWindow_h
#define packtpub_MainWindow_h


// Local includes
#include "PluginInterface.h"

// Qt includes
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QMainWindow>
#include <QMap>


namespace packtpub {


/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Default constructor.
     * @param parent Parent of this window.
     */
    explicit MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Default destructor.
     */
    virtual ~MainWindow();


    /**
     * @brief Setup the window.
     * @return True on success.
     */
    bool setup();


    /**
     * @brief Toogle image.
     * @param nextAction Flag to specify to going to the next image.
     */
    void toogleImage(const bool nextAction);




protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Initialise pointers.
     */
    void initPointers();


    /**
     * @brief Initialise values.
     */
    void initValues();


    /**
     * @brief Load plugin.
     */
    void loadPlugins();


    /**
     * @brief Setup actions.
     * @return True on success.
     */
    bool setupActions();


    /**
     * @brief Setup connections.
     * @return True on success.
     */
    bool setupConnections();


    /**
     * @brief Set default settings.
     * @return True on success.
     */
    bool setupDefaultSettings();


    /**
     * @brief Finalise the setup.
     * @return True on success.
     */
    bool setupFinalState();


    /**
     * @brief Setup shortcuts.
     * @return True on success.
     */
    bool setupShortcuts();


    /**
     * @brief Setup UIs.
     * @return True on success.
     */
    bool setupUis();


    /**
     * @brief Show image.
     * @param Path Path to image file to show.
     */
    void showImage(QString path);


    /**
     * @brief References.
     */
    QMenu* mFileMenu;
    QMenu* mEditMenu;
    QMenu* mViewMenu;
    QToolBar* mFileToolBar;
    QToolBar* mEditToolBar;
    QToolBar* mViewToolBar;
    QGraphicsScene* mImageScene;
    QGraphicsView* mImageView;
    QStatusBar* mStatusBar;
    QLabel* mMainStatus;

    QAction* mOpenAction;
    QAction* mSaveAsAction;
    QAction* mExitAction;

    QAction* mZoomInAction;
    QAction* mZoomOutAction;
    QAction* mPrevAction;
    QAction* mNextAction;

    QAction* mBlurAction;

    QString mCurrentPath;
    QGraphicsPixmapItem* mCurrentImage;

    QMap<QString, PluginInterface*> mPlugins;



protected slots:
    void cbBlurImage();
    void cbExit();
    void cbOpenImage();
    void cbNextImage();
    void cbPrevImage();
    void cbPreformPlugin();
    void cbSaveAs();
    void cbZoomIn();
    void cbZoomOut();


};  // class MainWindow


}  // namespace packtpub


#endif  // packtpub_MainWindow_h
