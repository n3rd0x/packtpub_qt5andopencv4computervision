/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"

// Qt includes
#include <QFileDialog>
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbOpenImage() {
    QFileDialog dialog(this);
    dialog.setWindowTitle(tr("Open Image"));
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));

    QStringList filePaths;
    if(dialog.exec()) {
        filePaths = dialog.selectedFiles();
        showImage(filePaths.at(0));
    }
}


void MainWindow::cbNextImage() {
    toogleImage(true);
}


void MainWindow::cbPrevImage() {
    toogleImage(false);
}


void MainWindow::cbSaveAs() {
    if(mCurrentImage == nullptr) {
        QMessageBox::information(this, tr("Information"), tr("Nothing to save."));
        return;
    }

    QFileDialog dialog(this);
    dialog.setWindowTitle(tr("Save Image As ..."));
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg"));

    QStringList fileNames;
    if(dialog.exec()) {
        fileNames = dialog.selectedFiles();
        if(QRegExp(".+\\.(png|bmp|jpg)").exactMatch(fileNames.at(0))) {
            mCurrentImage->pixmap().save(fileNames.at(0));
        }
        else {
            QMessageBox::information(this, tr("Information"), tr("Save error: bad format or filename."));
        }
    }
}


void MainWindow::cbZoomIn() {
    mImageView->scale(1.2, 1.2);
}


void MainWindow::cbZoomOut() {
    mImageView->scale(1.0 / 1.2, 1.0 / 1.2);
}


}  // namespace packtpub
