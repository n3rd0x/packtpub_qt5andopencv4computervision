/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QCameraInfo>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
    cbCloseCamera();
}


void MainWindow::initPointers() {
#ifdef GAZER_USE_QT_CAMERA
    mCamera           = nullptr;
    mCameraViewfinder = nullptr;
#endif
    mImageScene = nullptr;
    mImageView  = nullptr;
    mStatusBar  = nullptr;
    mMainStatus = nullptr;
    mCapturer   = nullptr;
    mDataLock   = nullptr;
}


void MainWindow::initValues() {
}


void MainWindow::populateSaveList() {
    QDir dir(Utilities::getDataPath());
    QStringList nameFilters;
    nameFilters << "*.jpg";

    QFileInfoList files = dir.entryInfoList(nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);
    for(auto cover : files) {
        auto name = cover.baseName();
        auto item = new QStandardItem();
        mListModel->appendRow(item);

        auto index = mListModel->indexFromItem(item);
        mListModel->setData(index, QPixmap(cover.absoluteFilePath()).scaledToHeight(145), Qt::DecorationRole);
        mListModel->setData(index, name, Qt::DisplayRole);
    }
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mActionExit, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mActionCalcFPS, &QAction::triggered, this, &MainWindow::cbCalculateFPS);
    connect(mActionCamInfo, &QAction::triggered, this, &MainWindow::cbShowCameraInfo);
    connect(mActionOpenCam, &QAction::triggered, this, &MainWindow::cbOpenCamera);
    connect(mChkMonitor, &QCheckBox::toggled, this, &MainWindow::cbUpdateMonitorStatus);
    connect(mBtnRecord, &QPushButton::clicked, this, &MainWindow::cbToggleRecording);

    return true;
}


bool MainWindow::setupDefaultSettings() {
    return true;
}


bool MainWindow::setupFinalState() {
    resize(1024, 756);
    setWindowTitle(qApp->applicationName());
    populateSaveList();
    return true;
}


bool MainWindow::setupUis() {
    // Setup UI defined in the designer.
    setupUi(this);

    auto layout = new QGridLayout(mDisplay);

    // Main area for image display.
#ifdef GAZER_USE_QT_CAMERA
    auto cameras = QCameraInfo::availableCameras();
    if(!cameras.empty()) {
        mCameraList->addItem("** Select Camera **");
        for(auto itr : cameras) {
            mCameraList->addItem(itr.deviceName());
        }
    }
    else {
        mCameraList->addItem("** No Camera Available **");
    }

    mCameraViewfinder = new QCameraViewfinder(this);
    layout->addWidget(mCameraViewfinder, 0, 0, 1, 1);
#else
    mImageScene = new QGraphicsScene(this);
    mImageView  = new QGraphicsView(mImageScene);
    layout->addWidget(mImageView, 0, 0, 1, 1);

    mCameraList->addItem("** Enter CameraID Below **");
#endif

    // List view.
    mListModel = new QStandardItemModel(this);
    mListView->setModel(mListModel);

    // Setup status bar.
    mMainStatus = new QLabel(mStatusBar);
    mStatusBar->addPermanentWidget(mMainStatus);
    mMainStatus->setText(tr("Gazer is Ready!"));
    return true;
}


}  // namespace packtpub
