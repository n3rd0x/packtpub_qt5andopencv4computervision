/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QCameraInfo>
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbAppendSaveVideo(const QString& name) {
    auto cover = Utilities::getSavedVideoPath(name, "jpg");
    auto item  = new QStandardItem();
    mListModel->appendRow(item);

    QModelIndex index = mListModel->indexFromItem(item);
    mListModel->setData(index, QPixmap(cover).scaledToHeight(145), Qt::DecorationRole);
    mListModel->setData(index, name, Qt::DisplayRole);
    mListView->scrollTo(index);
}


void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbCalculateFPS() {
    if(mCapturer) {
        mCapturer->startCalcFPS();
    }
}


void MainWindow::cbCloseCamera() {
#ifdef GAZER_USE_QT_CAMERA
    if(mCamera) {
        mCamera->stop();
        delete mCamera;
        mCamera = nullptr;
    }
#else
    if(mCapturer) {
        mCapturer->setRunning(false);
        disconnect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
        disconnect(mCapturer, &CaptureThread::sFpsChanged, this, &MainWindow::cbUpdateFPS);
        disconnect(mCapturer, &CaptureThread::sVideoSaved, this, &MainWindow::cbAppendSaveVideo);
        connect(mCapturer, &CaptureThread::finished, mCapturer, &CaptureThread::deleteLater);
    }
#endif
    mBtnRecord->setEnabled(false);
}


void MainWindow::cbOpenCamera() {
#ifdef GAZER_USE_QT_CAMERA
    if(mCameraList->currentIndex() < 1) {
        QMessageBox::information(this, tr("Information"), "No Camera is Selected!");
        return;
    }

    cbCloseCamera();

    auto cameras = QCameraInfo::availableCameras();
    mCamera      = new QCamera(cameras[mCameraList->currentIndex() - 1]);
    QCameraViewfinderSettings settings;
    settings.setResolution(QSize(800, 600));
    mCamera->setViewfinder(mCameraViewfinder);
    mCamera->setViewfinderSettings(settings);
    mCamera->setCaptureMode(QCamera::CaptureVideo);
    mCamera->start();
#else
    if(mCameraID->text().isEmpty()) {
        QMessageBox::information(this, tr("Information"), "No Camera or Video File is Selected!");
        return;
    }

    cbCloseCamera();

    mCapturer = new CaptureThread(mCameraID->text(), mDataLock);
    connect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
    connect(mCapturer, &CaptureThread::sFpsChanged, this, &MainWindow::cbUpdateFPS);
    connect(mCapturer, &CaptureThread::sVideoSaved, this, &MainWindow::cbAppendSaveVideo);

    mCapturer->start();

    mMainStatus->setText(QString("Capturing Camera (%1)").arg(mCameraID->text()));
    mChkMonitor->setCheckState(Qt::Unchecked);
    mBtnRecord->setEnabled(true);
#endif
}


void MainWindow::cbShowCameraInfo() {
    auto cameras = QCameraInfo::availableCameras();
    QString info = QString("Available Cameras:");
    for(auto itr : cameras) {
        info += "\n - " + itr.deviceName() + ": ";
        info += itr.description();
    }
    QMessageBox::information(this, tr("Cameras"), info);
}


void MainWindow::cbToggleRecording() {
    auto text = mBtnRecord->text();
    if(text == "Start Recording" && mCapturer) {
        mCapturer->setVideoSavingStatus(CaptureThread::VideoSavingStatus::STARTING);
        mBtnRecord->setText("Stop Recording");
        mChkMonitor->setCheckState(Qt::Unchecked);
        mChkMonitor->setEnabled(false);
    }
    else if(text == "Stop Recording" && mCapturer) {
        mCapturer->setVideoSavingStatus(CaptureThread::VideoSavingStatus::STOPPING);
        mBtnRecord->setText("Start Recording");
        mChkMonitor->setEnabled(true);
    }
}


void MainWindow::cbUpdateFrame(cv::Mat* mat) {
    {
        QMutexLocker lock(mDataLock);
        mCurrentFrame = *mat;
    }

    QImage frame(mCurrentFrame.data,
                 mCurrentFrame.cols,
                 mCurrentFrame.rows,
                 static_cast<qint32>(mCurrentFrame.step),
                 QImage::Format_RGB888);
    auto pixmap = QPixmap::fromImage(frame);

    mImageScene->clear();
    mImageView->resetMatrix();
    mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());
}


void MainWindow::cbUpdateFPS(qreal fps) {
    mMainStatus->setText(QString("FPS of current camera is %1").arg(fps));
}


void MainWindow::cbUpdateMonitorStatus(bool state) {
    if(!mCapturer) {
        return;
    }
    if(state) {
        mCapturer->setMotionDetectionStatus(true);
        mBtnRecord->setEnabled(false);
    }
    else {
        mCapturer->setMotionDetectionStatus(false);
        mBtnRecord->setEnabled(true);
    }
}


}  // namespace packtpub
