/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Utilities.h"

// Qt includes
#include <QObject>
#include <QApplication>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHostInfo>
#include <QDebug>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
QString Utilities::getDataPath() {
    QString path = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation)[0];
    QDir dir(path);
    dir.mkpath("Gazer");
    return dir.absoluteFilePath("Gazer");
}


QString Utilities::getSavedVideoPath(const QString& name, const QString& postfix) {
    return QString("%1/%2.%3").arg(getDataPath(), name, postfix);
}


QString Utilities::newSavedVideoName() {
    QDateTime time = QDateTime::currentDateTime();
    return time.toString("yyyyMMdd_HHmmss");
}


void Utilities::notifyMobile(qint32 cameraID) {
    // CHANGE endpoint TO YOURS HERE:
    // https://maker.ifttt.com/trigger/Motion-Detected-by-Gazer/with/key/-YOUR_KEY
    // QString endpoint = QUrl("https://maker.ifttt.com/YOUR_END PIOIN");
    QString endpoint        = "https://maker.ifttt.com/trigger/PacktpubGazer2019";
    QNetworkRequest request = QNetworkRequest(QUrl(endpoint));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QJsonObject json;
    json.insert("value1", QString("%1").arg(cameraID));
    json.insert("value2", QHostInfo::localHostName());

    QNetworkAccessManager nam;
    QNetworkReply* rep = nam.post(request, QJsonDocument(json).toJson());
    while(!rep->isFinished()) {
        qApp->processEvents();
    }

    // QString strReply = static_cast<QString>(rep->readAll());
    // qDebug() << "Test: " << strReply;
    rep->deleteLater();
}


}  // namespace packtpub
