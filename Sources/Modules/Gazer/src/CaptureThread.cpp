/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "CaptureThread.h"
#include "Utilities.h"

// Qt includes
#include <QTime>
#include <QtConcurrent>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
CaptureThread::CaptureThread(const QString& id, QMutex* lock) {
    mCameraID = id;
    mDataLock = lock;
    mRunning  = false;

    mCalculateFPS = false;
    mFps          = 0.0;

    mFrameWidth        = 0;
    mFrameHeight       = 0;
    mVideoSavingStatus = VideoSavingStatus::STOPPED;
    mSaveVideoName     = "";
    mVideoWriter       = nullptr;

    mMotionDetectingStatus = false;
    mMotionDetected        = false;
}


CaptureThread::~CaptureThread() {
}


void CaptureThread::calculateFPS(cv::VideoCapture& capture) {
    const qint32 count2Read = 100;
    cv::Mat frame;

    QTime timer;
    timer.start();
    for(auto i = 0; i < count2Read; i++) {
        capture >> frame;
    }

    qint32 elapsedMS = timer.elapsed();
    mFps             = count2Read / (elapsedMS * 0.001);
    mCalculateFPS    = false;
    Q_EMIT sFpsChanged(mFps);
}


void CaptureThread::motionDetect(cv::Mat& frame) {
    cv::Mat fgmask;
    mSegmentor->apply(frame, fgmask);
    if(fgmask.empty()) {
        return;
    }

    cv::threshold(fgmask, fgmask, 25, 255, cv::THRESH_BINARY);

    qint32 noiseSize = 9;
    cv::Mat kernel   = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(noiseSize, noiseSize));
    cv::erode(fgmask, fgmask, kernel);

    kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(noiseSize, noiseSize));
    cv::dilate(fgmask, fgmask, kernel, cv::Point(-1, -1), 3);

    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(fgmask, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);

    bool hasMotion = contours.size() > 0;
    if(!mMotionDetected && hasMotion) {
        mMotionDetected = true;
        setVideoSavingStatus(VideoSavingStatus::STARTING);
        if(mCameraID.length() == 1) {
            QtConcurrent::run(Utilities::notifyMobile, mCameraID.toInt());
        }
    }
    else if(mMotionDetected && !hasMotion) {
        mMotionDetected = false;
        setVideoSavingStatus(VideoSavingStatus::STOPPING);
    }

    cv::Scalar colour = cv::Scalar(0, 0, 255);
    for(size_t i = 0; i < contours.size(); i++) {
        cv::Rect rect = cv::boundingRect(contours[i]);
        cv::rectangle(frame, rect, colour, 1);
        // cv::drawContours(frame, contours, static_cast<qint32>(i), colour, 1);
    }
}


void CaptureThread::run() {
    LOG_S(INFO) << "---- CaptureThread START RUNNING ----";
    mRunning = true;

    qint32 camidx = -1;
    if(mCameraID.length() == 1) {
        camidx = mCameraID.toInt();
    }

    cv::VideoCapture capture;
    if(camidx >= 0) {
        capture.open(camidx);
        LOG_S(INFO) << "Open Camera (" << camidx << ").";
    }
    else {
        capture.open(mCameraID.toStdString());
        LOG_S(INFO) << "Open VideoFile (" << mCameraID << ").";
    }


    cv::Mat capFrame;
    mFrameWidth  = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
    mFrameHeight = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
    mSegmentor   = cv::createBackgroundSubtractorMOG2(500, 16, true);

    LOG_S(INFO) << "Resolution: " << mFrameWidth << "x" << mFrameHeight;
    while(mRunning) {
        capture >> capFrame;
        if(capFrame.empty()) {
            break;
        }

        if(mMotionDetectingStatus) {
            motionDetect(capFrame);
        }

        if(mVideoSavingStatus == VideoSavingStatus::STARTING) {
            LOG_S(INFO) << "Start recording.";
            startSavingVideo(capFrame);
        }

        if(mVideoSavingStatus == VideoSavingStatus::STARTED) {
            mVideoWriter->write(capFrame);
        }

        if(mVideoSavingStatus == VideoSavingStatus::STOPPING) {
            stopSavingVideo();
            LOG_S(INFO) << "Stop recording.";
        }

        cvtColor(capFrame, capFrame, cv::COLOR_BGR2RGB);
        {
            QMutexLocker lock(mDataLock);
            mFrame = capFrame;
        }
        Q_EMIT sFrameCaptured(&mFrame);
        if(mCalculateFPS) {
            calculateFPS(capture);
            LOG_S(INFO) << "FPS: " << mFps;
        }
    }

    capture.release();
    mRunning = false;
    LOG_S(INFO) << "---- CaptureThread  STOP RUNNING ----";
}


void CaptureThread::setRunning(const bool state) {
    mRunning = state;
}


void CaptureThread::setMotionDetectionStatus(const bool status) {
    mMotionDetectingStatus = status;
    mMotionDetected        = false;
    if(mVideoSavingStatus != VideoSavingStatus::STOPPED) {
        mVideoSavingStatus = VideoSavingStatus::STOPPING;
    }
}


void CaptureThread::setVideoSavingStatus(const VideoSavingStatus status) {
    mVideoSavingStatus = status;
}


void CaptureThread::startCalcFPS() {
    mCalculateFPS = true;
}


void CaptureThread::startSavingVideo(cv::Mat& firstFrame) {
    mSaveVideoName = Utilities::newSavedVideoName();

    // Save the cover image.
    auto cover = Utilities::getSavedVideoPath(mSaveVideoName, "jpg");
    cv::imwrite(cover.toStdString(), firstFrame);


    mVideoWriter       = new cv::VideoWriter(Utilities::getSavedVideoPath(mSaveVideoName, "avi").toStdString(),
                                       cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
                                       static_cast<bool>(mFps) ? mFps : 30,
                                       cv::Size(mFrameWidth, mFrameHeight));
    mVideoSavingStatus = VideoSavingStatus::STARTED;
}


void CaptureThread::stopSavingVideo() {
    mVideoSavingStatus = VideoSavingStatus::STOPPED;
    mVideoWriter->release();
    delete mVideoWriter;
    mVideoWriter = nullptr;
    Q_EMIT sVideoSaved(mSaveVideoName);
}


}  // namespace packtpub
