/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_CaptureThread_h
#define packtpub_CaptureThread_h


// Qt includes
#include <QMutex>
#include <QString>
#include <QThread>

// OpenCV includes
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video/background_segm.hpp>


namespace packtpub {


/**
 * @brief Capture threading.
 */
class CaptureThread : public QThread {
    Q_OBJECT
public:
    enum class VideoSavingStatus : qint32 { STARTING, STARTED, STOPPING, STOPPED };

    CaptureThread(const QString& id, QMutex* lock);
    ~CaptureThread() override;
    void setRunning(const bool state);
    void setVideoSavingStatus(const VideoSavingStatus status);
    void setMotionDetectionStatus(const bool status);
    void startCalcFPS();


protected:
    void calculateFPS(cv::VideoCapture& capture);
    void motionDetect(cv::Mat& frame);
    void run() override;
    void startSavingVideo(cv::Mat& firstFrame);
    void stopSavingVideo();

    QString mCameraID;
    QMutex* mDataLock;
    cv::Mat mFrame;
    bool mRunning;

    // FPS calculating.
    bool mCalculateFPS;
    qreal mFps;

    // Video saving.
    qint32 mFrameHeight;
    qint32 mFrameWidth;
    VideoSavingStatus mVideoSavingStatus;
    QString mSaveVideoName;
    cv::VideoWriter* mVideoWriter;

    // Motion analysis.
    bool mMotionDetectingStatus;
    bool mMotionDetected;
    cv::Ptr<cv::BackgroundSubtractorMOG2> mSegmentor;


signals:
    void sFrameCaptured(cv::Mat* mat);
    void sFpsChanged(qreal fps);
    void sVideoSaved(const QString& name);


};  // class CaptureThread


}  // namespace packtpub




#endif  // packtpub_CaptureThread_h
