/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



#ifndef packtpub_MainWindow_h
#define packtpub_MainWindow_h


// Local includes
#include "CaptureThread.h"

// Qt includes
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QMainWindow>
#include <QStandardItem>

#ifdef GAZER_USE_QT_CAMERA
#    include <QCameraViewfinder>
#    include <QCamera>
#endif
#include <ui_MainWindow.h>

// OpenCV includes
#include <opencv2/opencv.hpp>


namespace packtpub {


/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow, protected Ui::MainWindow {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Default constructor.
     * @param parent Parent of this window.
     */
    explicit MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Default destructor.
     */
    virtual ~MainWindow();


    /**
     * @brief Setup the window.
     * @return True on success.
     */
    bool setup();



protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Initialise pointers.
     */
    void initPointers();


    /**
     * @brief Initialise values.
     */
    void initValues();


    /**
     * @brief Populate save list.
     */
    void populateSaveList();


    /**
     * @brief Setup connections.
     * @return True on success.
     */
    bool setupConnections();


    /**
     * @brief Set default settings.
     * @return True on success.
     */
    bool setupDefaultSettings();


    /**
     * @brief Finalise the setup.
     * @return True on success.
     */
    bool setupFinalState();


    /**
     * @brief Setup UIs.
     * @return True on success.
     */
    bool setupUis();


    /**
     * @brief References.
     */
    QGraphicsScene* mImageScene;
    QGraphicsView* mImageView;
#ifdef GAZER_USE_QT_CAMERA
    QCamera* mCamera;
    QCameraViewfinder* mCameraViewfinder;
#endif
    QLabel* mMainStatus;
    QStandardItemModel* mListModel;

    cv::Mat mCurrentFrame;

    // Capture thread.
    QMutex* mDataLock;
    CaptureThread* mCapturer;


protected slots:
    void cbAppendSaveVideo(const QString& name);
    void cbExit();
    void cbCalculateFPS();
    void cbCloseCamera();
    void cbOpenCamera();
    void cbShowCameraInfo();
    void cbToggleRecording();
    void cbUpdateFrame(cv::Mat* mat);
    void cbUpdateFPS(qreal fps);
    void cbUpdateMonitorStatus(bool status);


};  // class MainWindow


}  // namespace packtpub


#endif  // packtpub_MainWindow_h
