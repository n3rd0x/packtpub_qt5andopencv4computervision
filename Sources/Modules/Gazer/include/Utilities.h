/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_Utilities_h
#define packtpub_Utilities_h


// Qt includes
#include <QString>


namespace packtpub {


/**
 * @brief Utility.
 */
class Utilities {
public:
    static QString getDataPath();
    static QString getSavedVideoPath(const QString& name, const QString& postfix);
    static QString newSavedVideoName();
    static void notifyMobile(qint32 cameraID);


};  // class Utilities


}  // namespace packtpub


#endif  // packtpub_Utilities_h
