/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QCameraInfo>
#include <QMessageBox>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// GUI Callbacks
// ************************************************************
void MainWindow::cbAppendSavePhoto(const QString& name) {
    auto cover = Utilities::getPhotoPath(name, "jpg");
    auto item  = new QStandardItem();
    mListModel->appendRow(item);

    QModelIndex index = mListModel->indexFromItem(item);
    mListModel->setData(index, QPixmap(cover).scaledToHeight(145), Qt::DecorationRole);
    mListModel->setData(index, name, Qt::DisplayRole);
    mListView->scrollTo(index);
}


void MainWindow::cbCameraSelected(qint32 idx) {
    if(idx > 0) {
        mCameraID->setText(QString::number(idx - 1));
    }
}


void MainWindow::cbExit() {
    QWidget::close();
}


void MainWindow::cbCloseCamera() {
    if(mCapturer) {
        mCapturer->setRunning(false);
        disconnect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
        disconnect(mCapturer, &CaptureThread::sPhotoTaken, this, &MainWindow::cbAppendSavePhoto);
        connect(mCapturer, &CaptureThread::finished, mCapturer, &CaptureThread::deleteLater);
    }
    mChkGlasses->setEnabled(false);
    mChkMustache->setEnabled(false);
    mChkLandmarks->setEnabled(false);
    mChkLandmarkLabels->setEnabled(false);
    mChkMouseMose->setEnabled(false);
    mChkRectangle->setEnabled(false);
    mBtnTakePhoto->setEnabled(false);
}


void MainWindow::cbOpenCamera() {
    if(mCameraID->text().isEmpty()) {
        QMessageBox::information(this, tr("Information"), "No Camera or Video File is Selected!");
        return;
    }

    cbCloseCamera();

    mCapturer = new CaptureThread(mCameraID->text(), mDataLock);
    connect(mCapturer, &CaptureThread::sFrameCaptured, this, &MainWindow::cbUpdateFrame);
    connect(mCapturer, &CaptureThread::sPhotoTaken, this, &MainWindow::cbAppendSavePhoto);

    mCapturer->updateMasksFlag(CaptureThread::MaskType::GLASSES, mChkGlasses->isChecked());
    mCapturer->updateMasksFlag(CaptureThread::MaskType::MUSTACE, mChkMustache->isChecked());
    mCapturer->updateMasksFlag(CaptureThread::MaskType::LANDMARKS, mChkLandmarks->isChecked());
    mCapturer->updateMasksFlag(CaptureThread::MaskType::LANDMARK_LABELS, mChkLandmarkLabels->isChecked());
    mCapturer->updateMasksFlag(CaptureThread::MaskType::MOUSE_NOSE, mChkMouseMose->isChecked());
    mCapturer->updateMasksFlag(CaptureThread::MaskType::RECTANGLE, mChkRectangle->isChecked());

    mCapturer->start();

    mMainStatus->setText(QString("Capturing Camera (%1)").arg(mCameraID->text()));
    mChkGlasses->setEnabled(true);
    mChkMustache->setEnabled(true);
    mChkLandmarks->setEnabled(true);
    mChkLandmarkLabels->setEnabled(true);
    mChkMouseMose->setEnabled(true);
    mChkRectangle->setEnabled(true);
    mBtnTakePhoto->setEnabled(true);
}


void MainWindow::cbShowCameraInfo() {
    auto cameras = QCameraInfo::availableCameras();
    QString info = QString("Available Cameras:");
    for(auto itr : cameras) {
        info += "\n - " + itr.deviceName() + ": ";
        info += itr.description();
    }
    QMessageBox::information(this, tr("Cameras"), info);
}


void MainWindow::cbTakePhoto() {
    if(mCapturer) {
        mCapturer->takePhoto();
    }
}


void MainWindow::cbUpdateFrame(cv::Mat* mat) {
    {
        QMutexLocker lock(mDataLock);
        mCurrentFrame = *mat;
    }

    QImage frame(mCurrentFrame.data,
                 mCurrentFrame.cols,
                 mCurrentFrame.rows,
                 static_cast<qint32>(mCurrentFrame.step),
                 QImage::Format_RGB888);
    auto pixmap = QPixmap::fromImage(frame);

    mImageScene->clear();
    mImageView->resetMatrix();
    mImageScene->addPixmap(pixmap);
    mImageScene->update();
    mImageView->setSceneRect(pixmap.rect());
}


void MainWindow::cbUpdateMask(qint32 status) {
    if(!mCapturer) {
        return;
    }

    auto box = qobject_cast<QCheckBox*>(sender());
    if(box == mChkGlasses) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::GLASSES, status != 0);
    }
    else if(box == mChkMustache) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::MUSTACE, status != 0);
    }
    else if(box == mChkLandmarks) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::LANDMARKS, status != 0);
    }
    else if(box == mChkLandmarkLabels) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::LANDMARK_LABELS, status != 0);
    }
    else if(box == mChkMouseMose) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::MOUSE_NOSE, status != 0);
    }
    else if(box == mChkRectangle) {
        mCapturer->updateMasksFlag(CaptureThread::MaskType::RECTANGLE, status != 0);
    }
}


}  // namespace packtpub
