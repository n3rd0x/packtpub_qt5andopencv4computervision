/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "CaptureThread.h"
#include "Utilities.h"

// Qt includes
#include <QApplication>
#include <QImage>
#include <QTime>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
CaptureThread::CaptureThread(const QString& id, QMutex* lock) {
    mCameraID    = id;
    mDataLock    = lock;
    mRunning     = false;
    mTakingPhoto = false;

    mFrameWidth  = 0;
    mFrameHeight = 0;

    loadOrnaments();

    mMaskFlag = 0;
}


CaptureThread::~CaptureThread() {
}


void CaptureThread::detectFaces(cv::Mat& frame) {
    std::vector<cv::Rect> faces;
    cv::Mat grayFrame;
    cv::cvtColor(frame, grayFrame, cv::COLOR_BGR2GRAY);
    mClassifier->detectMultiScale(grayFrame, faces, 1.3, 5);

    cv::Scalar colour = cv::Scalar(0, 0, 255);

    // Draw the circumscribe rectangles.
    if(isMaskOn(MaskType::RECTANGLE)) {
        for(size_t i = 0; i < faces.size(); i++) {
            cv::rectangle(frame, faces[i], colour, 1);
        }
    }

    std::vector<std::vector<cv::Point2f>> shapes;
    if(mMarkDetector->fit(frame, faces, shapes)) {
        // Draw facial land marks.
        for(unsigned long i = 0; i < faces.size(); i++) {
            if(isMaskOn(MaskType::LANDMARKS)) {
                for(unsigned long k = 0; k < shapes[k].size(); k++) {
                    cv::circle(frame, shapes[i][k], 2, colour, cv::FILLED);

                    if(isMaskOn(MaskType::LANDMARK_LABELS)) {
                        QString index = QString("%1").arg(k);
                        cv::putText(frame,
                                    index.toStdString(),
                                    shapes[i][k],
                                    cv::FONT_HERSHEY_SIMPLEX,
                                    0.2,
                                    cv::Scalar(255, 0, 0),
                                    2);
                    }
                }
            }
            if(isMaskOn(MaskType::GLASSES)) {
                drawGlasses(frame, shapes[i]);
            }
            if(isMaskOn(MaskType::MUSTACE)) {
                drawMustache(frame, shapes[i]);
            }
            if(isMaskOn(MaskType::MOUSE_NOSE)) {
                drawMouseNose(frame, shapes[i]);
            }
        }
    }
}


void CaptureThread::drawGlasses(cv::Mat& frame, std::vector<cv::Point2f>& marks) {
    // Resize.
    cv::Mat ornament;
    qreal distance = cv::norm(marks[45] - marks[36]) * 1.5;
    cv::resize(
        mGlasses, ornament, cv::Size(0, 0), distance / mGlasses.cols, distance / mGlasses.cols, cv::INTER_NEAREST);

    // Rotate.
    qreal angle        = static_cast<qreal>(-atan((marks[45].y - marks[36].y) / (marks[45].x - marks[36].x)));
    cv::Point2f center = cv::Point(ornament.cols / 2, ornament.rows / 2);
    cv::Mat rotMat     = cv::getRotationMatrix2D(center, angle * 180.0 / 3.14, 1.0);

    cv::Mat rotated;
    cv::warpAffine(
        ornament, rotated, rotMat, ornament.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(255, 255, 255));

    // Paint.
    center = cv::Point((marks[45].x + marks[36].x) / 2, (marks[45].y + marks[36].y) / 2);
    cv::Rect rect(center.x - rotated.cols / 2, center.y - rotated.rows / 2, rotated.cols, rotated.rows);
    frame(rect) &= rotated;
}


void CaptureThread::drawMustache(cv::Mat& frame, std::vector<cv::Point2f>& marks) {
    // Resize.
    cv::Mat ornament;
    qreal distance = cv::norm(marks[54] - marks[48]) * 1.5;
    cv::resize(
        mMustache, ornament, cv::Size(0, 0), distance / mMustache.cols, distance / mMustache.cols, cv::INTER_NEAREST);

    // Rotate.
    qreal angle        = static_cast<qreal>(-atan((marks[54].y - marks[48].y) / (marks[54].x - marks[48].x)));
    cv::Point2f center = cv::Point(ornament.cols / 2, ornament.rows / 2);
    cv::Mat rotMat     = cv::getRotationMatrix2D(center, angle * 180.0 / 3.14, 1.0);

    cv::Mat rotated;
    cv::warpAffine(
        ornament, rotated, rotMat, ornament.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(255, 255, 255));

    // Paint.
    center = cv::Point((marks[33].x + marks[51].x) / 2, (marks[33].y + marks[51].y) / 2);
    cv::Rect rect(center.x - rotated.cols / 2, center.y - rotated.rows / 2, rotated.cols, rotated.rows);
    frame(rect) &= rotated;
}


void CaptureThread::drawMouseNose(cv::Mat& frame, std::vector<cv::Point2f>& marks) {
    // Resize.
    cv::Mat ornament;
    qreal distance = cv::norm(marks[13] - marks[3]) * 1.5;
    cv::resize(mMouseNose,
               ornament,
               cv::Size(0, 0),
               distance / mMouseNose.cols,
               distance / mMouseNose.cols,
               cv::INTER_NEAREST);

    // Rotate.
    qreal angle        = static_cast<qreal>(-atan((marks[16].y - marks[0].y) / (marks[16].x - marks[0].x)));
    cv::Point2f center = cv::Point(ornament.cols / 2, ornament.rows / 2);
    cv::Mat rotMat     = cv::getRotationMatrix2D(center, angle * 180.0 / 3.14, 1.0);

    cv::Mat rotated;
    cv::warpAffine(
        ornament, rotated, rotMat, ornament.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(255, 255, 255));

    // Paint.
    center = marks[30];
    cv::Rect rect(center.x - rotated.cols / 2, center.y - rotated.rows / 2, rotated.cols, rotated.rows);
    frame(rect) &= rotated;
}


bool CaptureThread::isMaskOn(MaskType type) {
    return (mMaskFlag & (1 << static_cast<quint8>(type))) != 0;
}


void CaptureThread::loadOrnaments() {
    QImage image;
    image.load(":/images/glasses.jpg");
    image    = image.convertToFormat(QImage::Format_RGB888);
    mGlasses = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), static_cast<size_t>(image.bytesPerLine()))
                   .clone();

    image.load(":/images/mustache.jpg");
    image     = image.convertToFormat(QImage::Format_RGB888);
    mMustache = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), static_cast<size_t>(image.bytesPerLine()))
                    .clone();

    image.load(":/images/mouse-nose.jpg");
    image = image.convertToFormat(QImage::Format_RGB888);
    mMouseNose
        = cv::Mat(image.height(), image.width(), CV_8UC3, image.bits(), static_cast<size_t>(image.bytesPerLine()))
              .clone();
}


void CaptureThread::run() {
    LOG_S(INFO) << "---- CaptureThread START RUNNING ----";
    mRunning = true;

    QTime timer;
    qint32 camidx = -1;
    if(mCameraID.length() == 1) {
        camidx = mCameraID.toInt();
    }

    cv::VideoCapture capture;
    if(camidx >= 0) {
        capture.open(camidx);
        LOG_S(INFO) << "Open Camera (" << camidx << ").";
    }
    else {
        capture.open(mCameraID.toStdString());
        LOG_S(INFO) << "Open VideoFile (" << mCameraID << ").";
        timer.start();
    }

    capture.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    capture.set(cv::CAP_PROP_FRAME_HEIGHT, 480);


    cv::Mat capFrame;
    mFrameWidth  = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
    mFrameHeight = static_cast<qint32>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
    mClassifier  = new cv::CascadeClassifier("data/haarcascades/haarcascade_frontalface_default.xml");

    std::string faceModel = "data/lbfmodel.yaml";
    mMarkDetector         = cv::face::createFacemarkLBF();
    mMarkDetector->loadModel(faceModel);

    LOG_S(INFO) << "Resolution: " << mFrameWidth << "x" << mFrameHeight;
    LOG_S(INFO) << "Model:      " << faceModel;
    while(mRunning) {
        capture >> capFrame;
        if(capFrame.empty()) {
            break;
        }

        if(mMaskFlag > 0) {
            detectFaces(capFrame);
        }

        if(mTakingPhoto) {
            takePhoto(capFrame);
        }

        cvtColor(capFrame, capFrame, cv::COLOR_BGR2RGB);
        {
            QMutexLocker lock(mDataLock);
            mFrame = capFrame;
        }
        Q_EMIT sFrameCaptured(&mFrame);

        // Constrains to 25 FPS if open a video file.
        if(camidx < 0) {
            // Sec / Fps * ms
            // (1 / 30) * 1000
            auto time   = 33;
            auto elapse = timer.elapsed();
            if(elapse < time) {
                time = time - elapse;
            }
            else {
                time = 0;
            }
            msleep(time);
            timer.restart();
        }
    }

    capture.release();

    delete mClassifier;
    mClassifier = nullptr;

    mRunning = false;
    LOG_S(INFO) << "---- CaptureThread  STOP RUNNING ----";
}


void CaptureThread::setRunning(const bool state) {
    mRunning = state;
}


void CaptureThread::takePhoto() {
    mTakingPhoto = true;
}


void CaptureThread::takePhoto(cv::Mat& frame) {
    auto name = Utilities::newPhotoName();
    auto path = Utilities::getPhotoPath(name, "jpg");

    cv::imwrite(path.toStdString(), frame);
    Q_EMIT sPhotoTaken(name);
    mTakingPhoto = false;
}


void CaptureThread::updateMasksFlag(MaskType type, const bool value) {
    quint8 bit = 1 << static_cast<qint8>(type);
    if(value) {
        mMaskFlag |= bit;
    }
    else {
        mMaskFlag &= ~bit;
    }
}


}  // namespace packtpub
