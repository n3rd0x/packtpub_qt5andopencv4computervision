/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"
#include "Utilities.h"

// Qt includes
#include <QCameraInfo>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
    cbCloseCamera();
}


void MainWindow::initPointers() {
    mImageScene = nullptr;
    mImageView  = nullptr;
    mMainStatus = nullptr;
    mCapturer   = nullptr;
    mDataLock   = nullptr;
}


void MainWindow::initValues() {
}


void MainWindow::populateSaveList() {
    QDir dir(Utilities::getDataPath());
    QStringList nameFilters;
    nameFilters << "*.jpg";

    QFileInfoList files = dir.entryInfoList(nameFilters, QDir::NoDotAndDotDot | QDir::Files, QDir::Name);
    for(auto cover : files) {
        auto name = cover.baseName();
        auto item = new QStandardItem();
        mListModel->appendRow(item);

        auto index = mListModel->indexFromItem(item);
        mListModel->setData(index, QPixmap(cover.absoluteFilePath()).scaledToHeight(145), Qt::DecorationRole);
        mListModel->setData(index, name, Qt::DisplayRole);
    }
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mActionExit, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mActionCamInfo, &QAction::triggered, this, &MainWindow::cbShowCameraInfo);
    connect(mActionOpenCam, &QAction::triggered, this, &MainWindow::cbOpenCamera);
    connect(mBtnOpenCamera, &QPushButton::clicked, this, &MainWindow::cbOpenCamera);
    connect(mChkRectangle,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mChkGlasses,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mChkMustache,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mChkLandmarks,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mChkLandmarkLabels,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mChkMouseMose,
            static_cast<void (QCheckBox::*)(qint32)>(&QCheckBox::stateChanged),
            this,
            &MainWindow::cbUpdateMask);
    connect(mBtnTakePhoto, &QPushButton::clicked, this, &MainWindow::cbTakePhoto);
    connect(mCameraList,
            static_cast<void (QComboBox::*)(qint32)>(&QComboBox::currentIndexChanged),
            this,
            &MainWindow::cbCameraSelected);

    return true;
}


bool MainWindow::setupDefaultSettings() {
    return true;
}


bool MainWindow::setupFinalState() {
    resize(1024, 756);
    setWindowTitle(qApp->applicationName());
    populateSaveList();
    return true;
}


bool MainWindow::setupUis() {
    // Setup UI defined in the designer.
    setupUi(this);

    auto layout = new QGridLayout(mDisplay);
    mImageScene = new QGraphicsScene(this);
    mImageView  = new QGraphicsView(mImageScene);
    layout->addWidget(mImageView, 0, 0, 1, 1);

    // List cameras.
    mCameraList->addItem("** Select Camera **");
    auto cameras = QCameraInfo::availableCameras();
    for(auto itr : cameras) {
        mCameraList->addItem(itr.description());
    }

    // List view.
    mListModel = new QStandardItemModel(this);
    mListView->setModel(mListModel);

    // Setup status bar.
    mMainStatus = new QLabel(mStatusBar);
    mStatusBar->addPermanentWidget(mMainStatus);
    mMainStatus->setText(tr("Facetious is Ready!"));
    return true;
}


}  // namespace packtpub
