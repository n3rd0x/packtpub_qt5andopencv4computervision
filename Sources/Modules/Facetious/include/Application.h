/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_Application_h
#define packtpub_Application_h


// Local includes
#include "MainWindow.h"

// Qt includes
#include <QApplication>
#include <QScopedPointer>


namespace packtpub {


/**
 * @brief Main application.
 */
class Application : public QApplication {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Constructor.
     * @param argc Number of arguments.
     * @param argv Pointer to the arguments.
     */
    Application(int& argc, char** argv);


    /**
     * @brief Destructor.
     */
    virtual ~Application();


    /**
     * @brief Main loop.
     * @return Exit code.
     */
    int run();


    /**
     * @brief Shutdown.
     */
    void shutDown();


    /**
     * @brief Startup.
     * @return True on success.
     */
    bool startUp();



protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief References.
     */
    QScopedPointer<MainWindow> mMainWindow;




};  // class Application


}  // namespace packtpub


#endif  // packtpub_Application_h
