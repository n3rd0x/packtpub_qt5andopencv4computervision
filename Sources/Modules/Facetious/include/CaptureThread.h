/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_CaptureThread_h
#define packtpub_CaptureThread_h


// Qt includes
#include <QMutex>
#include <QString>
#include <QThread>

// OpenCV includes
#include <opencv2/opencv.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/face/facemark.hpp>

// STL includes
#include <vector>


namespace packtpub {


/**
 * @brief Capture threading.
 */
class CaptureThread : public QThread {
    Q_OBJECT
public:
    enum class MaskType : quint8 {
        RECTANGLE = 0,
        LANDMARKS,
        LANDMARK_LABELS,
        GLASSES,
        MUSTACE,
        MOUSE_NOSE,
        MASK_COUNT
    };

    CaptureThread(const QString& id, QMutex* lock);
    ~CaptureThread() override;
    void setRunning(const bool state);
    void takePhoto();
    void updateMasksFlag(MaskType type, const bool value);


protected:
    void detectFaces(cv::Mat& frame);
    void drawGlasses(cv::Mat& frame, std::vector<cv::Point2f>& marks);
    void drawMustache(cv::Mat& frame, std::vector<cv::Point2f>& marks);
    void drawMouseNose(cv::Mat& frame, std::vector<cv::Point2f>& marks);
    void loadOrnaments();
    bool isMaskOn(MaskType type);
    void run() override;
    void takePhoto(cv::Mat& frame);


    QString mCameraID;
    QMutex* mDataLock;
    cv::Mat mFrame;
    bool mRunning;

    qint32 mFrameHeight;
    qint32 mFrameWidth;
    bool mTakingPhoto;

    // Face analysis.
    cv::CascadeClassifier* mClassifier;
    cv::Ptr<cv::face::Facemark> mMarkDetector;

    // Mask ornaments.
    cv::Mat mGlasses;
    cv::Mat mMustache;
    cv::Mat mMouseNose;

    quint8 mMaskFlag;


signals:
    void sFrameCaptured(cv::Mat* mat);
    void sPhotoTaken(const QString& name);


};  // class CaptureThread


}  // namespace packtpub


#endif  // packtpub_CaptureThread_h
