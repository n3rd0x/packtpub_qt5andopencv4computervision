/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



// Local includes
#include "MainWindow.h"

// Qt includes
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    initPointers();
    initValues();
}


MainWindow::~MainWindow() {
}


void MainWindow::initPointers() {
}


void MainWindow::initValues() {
}


bool MainWindow::setup() {
    if(!setupUis()) {
        return false;
    }
    if(!setupConnections()) {
        return false;
    }
    if(!setupDefaultSettings()) {
        return false;
    }
    if(!setupFinalState()) {
        return false;
    }
    return true;
}


bool MainWindow::setupConnections() {
    // ----------------------------------------
    // GUI
    // ----------------------------------------
    connect(mActionExit, &QAction::triggered, this, &MainWindow::cbExit);
    connect(mActionUseFilter,
            static_cast<void (QAction::*)(bool)>(&QAction::triggered),
            mGLWidget,
            &GLWidget::setFilerUsage);
    connect(mActionSaveImage, &QAction::triggered, mGLWidget, &GLWidget::setSaveOutput);
    return true;
}


bool MainWindow::setupDefaultSettings() {
    return true;
}


bool MainWindow::setupFinalState() {
    resize(1024, 756);
    setWindowTitle(qApp->applicationName());
    return true;
}


bool MainWindow::setupUis() {
    // Setup UI defined in the designer.
    setupUi(this);
    return true;
}


}  // namespace packtpub
