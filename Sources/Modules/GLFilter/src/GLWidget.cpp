/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "Prerequisites.h"
#include "GLWidget.h"

// Qt includes
#include <QFile>


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
GLWidget::GLWidget(QWidget* parent) : QOpenGLWidget(parent) {
    mUseFilter  = 1;
    mSaveOutput = false;
}


GLWidget::~GLWidget() {
}


void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    // clang-format off
    GLfloat points[] = {
        // [Position]               // [Texture Coordinate]
        // ====================
        // = Triangle left
        // ====================
        -1.0f, -1.0f, 0.0f,         0.0f, 1.0f, // Left-Bottom
        -1.0f,  1.0f, 0.0f,         0.0f, 0.0f, // Left-Top
         1.0f,  1.0f, 0.0f,         1.0f, 0.0f, // Right-Top
        // ====================
        // = Triangle Right
        // ====================
        -1.0f, -1.0f, 0.0f,         0.0f, 1.0f, // Left-Bottom
         1.0f,  1.0f, 0.0f,         1.0f, 0.0f, // Right-Top
         1.0f, -1.0f, 0.0f,         1.0f, 1.0f  // Right-Bottom
    };
    // clang-format on

    // VBA & VAO.
    mVao.create();
    mVao.bind();

    mVbo.create();
    mVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    mVbo.bind();
    mVbo.allocate(points, sizeof(points) * sizeof(GLfloat));

    // Vertex.
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), nullptr);

    // Texture.
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<void*>(3 * sizeof(float)));

    // Texture
    glEnable(GL_TEXTURE_2D);

    // 1. Read the image data.
    uint8_t* image_data  = nullptr;
    int32_t image_width  = 0;
    int32_t image_height = 0;
#ifdef GLFILTER_USE_OPENCV
    mImage = cv::imread("data/lizard.jpg");
    cvtColor(mImage, mImage, cv::COLOR_BGR2RGB);
    image_width  = mImage.cols;
    image_height = mImage.rows;
    image_data   = mImage.data;
#else
    mImage       = QImage("data/lizard.jpg");
    mImage       = mImage.convertToFormat(QImage::Format_RGB888);
    image_width  = mImage.width();
    image_height = mImage.height();
    image_data   = mImage.bits();
#endif


    // 2. Generate texture.
    glGenTextures(1, &mTex);
    glBindTexture(GL_TEXTURE_2D, mTex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image_width, image_height, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Shaders.
    mShader.reset(new QOpenGLShaderProgram());
    mShader->create();
    mShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "data/shaders/vertex.shader");
    mShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "data/shaders/fragment.shader");
    mShader->bindAttributeLocation("aVertex", 0);
    mShader->bindAttributeLocation("aTexCoord", 1);
    mShader->link();
    mShader->bind();
    mShader->setUniformValue("uPixelScale", 1.0f / image_width, 1.0f / image_height);
    mShader->setUniformValue("uFilter", mUseFilter);

    mVbo.release();
    mVao.release();
    mShader->release();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glClearColor(0.1f, 0.1f, 0.2f, 1.0f);

    this->parentWidget()->parentWidget()->resize(image_width, image_height);
}


void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    // glViewport(0, 0, static_cast<GLsizei>(mImage.width()), static_cast<GLsizei>(mImage.height()));


    glBindTexture(GL_TEXTURE_2D, mTex);

    mShader->bind();
    mShader->setUniformValue("uFilter", mUseFilter);

    mVao.bind();
    glDrawArrays(GL_TRIANGLES, 0, 6);

    if(mSaveOutput) {
        saveOutputImage("data/GLFilter_Output.jpg");
        mSaveOutput = false;
    }

    mVao.release();
    mShader->release();
}


void GLWidget::resizeGL(int w, int h) {
    glViewport(0, 0, static_cast<GLsizei>(w), static_cast<GLsizei>(h));
}


void GLWidget::saveOutputImage(const QString& file) {
#ifdef GLFILTER_USE_OPENCV
    cv::Mat output(mImage.rows, mImage.cols, CV_8UC3);
    glReadPixels(0, 0, mImage.cols, mImage.rows, GL_RGB, GL_UNSIGNED_BYTE, output.data);

    cv::Mat cpy;
    cv::flip(output, cpy, 0);
    cvtColor(cpy, output, cv::COLOR_RGB2BGR);
    cv::imwrite(file.toStdString(), output);
#else
    //    QImage output(mImage.width(), mImage.height(), QImage::Format_RGB888);
    //    glReadPixels(0, 0, mImage.width(), mImage.height(), GL_RGB, GL_UNSIGNED_BYTE, output.bits());
    //    output = output.mirrored(false, true);
    //    output.save(file);
    QImage output(width(), height(), QImage::Format_RGB888);
    glReadPixels(0, 0, width(), height(), GL_RGB, GL_UNSIGNED_BYTE, output.bits());
    output = output.mirrored(false, true);
    output.save(file);
#endif
}


void GLWidget::setFilerUsage(bool state) {
    mUseFilter = static_cast<GLint>(state);
    update();
}


void GLWidget::setSaveOutput() {
    mSaveOutput = true;
    update();
}


// std::string GLWidget::textContent(const QString& file) {
//    QFile inFile(file);
//    if(inFile.open(QFile::ReadOnly | QFile::Text)) {
//        QTextStream in(&inFile);
//        return in.readAll().toStdString();
//    }
//    return "";
//}


}  // namespace packtpub
