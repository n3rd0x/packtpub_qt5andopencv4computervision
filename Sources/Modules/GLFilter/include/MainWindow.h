/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */



#ifndef packtpub_MainWindow_h
#define packtpub_MainWindow_h


// Local includes


// Qt includes
#include <QMainWindow>

// Generated includes
#include <ui_MainWindow.h>


namespace packtpub {


/**
 * @brief The main window.
 */
class MainWindow : public QMainWindow, protected Ui::MainWindow {
    Q_OBJECT

public:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Default constructor.
     * @param parent Parent of this window.
     */
    explicit MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Default destructor.
     */
    virtual ~MainWindow();


    /**
     * @brief Setup the window.
     * @return True on success.
     */
    bool setup();



protected:
    // ************************************************************
    // Member Declarations
    // ************************************************************
    /**
     * @brief Initialise pointers.
     */
    void initPointers();


    /**
     * @brief Initialise values.
     */
    void initValues();


    /**
     * @brief Setup connections.
     * @return True on success.
     */
    bool setupConnections();


    /**
     * @brief Set default settings.
     * @return True on success.
     */
    bool setupDefaultSettings();


    /**
     * @brief Finalise the setup.
     * @return True on success.
     */
    bool setupFinalState();


    /**
     * @brief Setup UIs.
     * @return True on success.
     */
    bool setupUis();




protected slots:
    void cbExit();


};  // class MainWindow


}  // namespace packtpub


#endif  // packtpub_MainWindow_h
