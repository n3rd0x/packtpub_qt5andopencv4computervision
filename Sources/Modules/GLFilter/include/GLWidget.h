/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_GLWidget_h
#define packtpub_GLWidget_h


// Qt includes
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>
#include <QScopedPointer>
#include <QString>
#include <QImage>

#ifdef GLFILTER_USE_OPENCV
// OpenCV includes
#    include <opencv2/opencv.hpp>
#endif


namespace packtpub {


/**
 * @brief OpenGL rendering widget.
 */
class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT
public:
    GLWidget(QWidget* parent = nullptr);
    ~GLWidget() override;


public slots:
    void setFilerUsage(bool state);
    void setSaveOutput();


protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void saveOutputImage(const QString& file);
    // std::string textContent(const QString& file);

    QScopedPointer<QOpenGLShaderProgram> mShader;
    QOpenGLVertexArrayObject mVao;
    QOpenGLBuffer mVbo;
    GLuint mTex;
    GLint mUseFilter;
    bool mSaveOutput;

#ifdef GLFILTER_USE_OPENCV
    cv::Mat mImage;
#else
    QImage mImage;
#endif


};  // class GLWidget


}  // namespace packtpub


#endif  // packtpub_GLWidget_h
