/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


// Local includes
#include "PluginInterface.h"


namespace packtpub {


// ************************************************************
// Class Implementations
// ************************************************************
PluginInterface::PluginInterface(QObject* parent) : QObject(parent) {
}


PluginInterface::~PluginInterface() {
}


// QString PluginInterface::name() {
//    return "Undefined Plugin";
//}


// void PluginInterface::edit(const cv::Mat& input, cv::Mat& output) {
//}


}  // namespace packtpub
