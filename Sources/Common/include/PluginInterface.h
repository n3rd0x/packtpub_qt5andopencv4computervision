/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_PluginInterface_h
#define packtpub_PluginInterface_h


// Qt includes
#include <QObject>
#include <QString>
#include <QtPlugin>

// OpenCV includes
#include <opencv2/opencv.hpp>


namespace packtpub {


/**
 * @brief Plugin Interface.
 */
class PluginInterface : public QObject {
    Q_OBJECT
public:
    PluginInterface(QObject* parent = nullptr);
    virtual ~PluginInterface();
    virtual QString name()                                   = 0;
    virtual void edit(const cv::Mat& input, cv::Mat& output) = 0;



};  // class PluginInterface


}  // namespace packtpub




// ************************************************************
// Plug-in Declarations
// ************************************************************
Q_DECLARE_INTERFACE(packtpub::PluginInterface, "packtpub.PluginInterface")


#endif  // packtpub_PluginInterface_h
