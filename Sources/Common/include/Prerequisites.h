/**
 * Qt5 and OpenCV 4 Computer Vision
 * Ref: https://www.packtpub.com/data/qt-5-and-opencv-4-computer-vision-projects
 */


#ifndef packtpub_Prerequisites_h
#define packtpub_Prerequisites_h


// Loguru includes
#include "loguru.hpp"


// Debug definition check.
#ifdef _WIN32
#    if defined(_DEBUG) || defined(DEBUG)
#        define DEBUG_VERSION 1
#    else
#        define DEBUG_VERSION 0
#    endif
#else
#    ifndef NDEBUG
#        define DEBUG_VERSION 1
#    else
#        define DEBUG_VERSION 0
#    endif
#endif


#if LOGURU_DEBUG_LOGGING
#    define DLOG_SCOPE_F(verbosity_name, ...) LOG_SCOPE_F(verbosity_name, __VA_ARGS__)
#else
#    define DLOG_SCOPE_F(verbosity_name, ...)
#endif


#endif  // packtpub_Prerequisites_h
